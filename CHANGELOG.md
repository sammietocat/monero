# Change Log

## [2017-08-10]
+ **Added**
    - demo for a full RCT instance in file `sammy/tx/tx-full-demo.cpp`
+ **Updated**
    - fix all bugs due to update of source files in `sammy/tx` folder

## [2017-08-09]  
+ **Added**  
	- demo for a simple RCT instance in file `sammy/tx/tx-simple-demo.cpp`

## [2017-08-08]
+ **Added**
    - demo for simple MLSAG and RCT signature

## [2017-08-04]
+ **Added**
    - a demo for preparing input coins, as `sammy/tx/source_gen.cpp`

## [2017-08-02]
+ **Added**
    - doc for `wallet2::create_transactions_2`

## [2017-07-27]  
+ **Added**  
	- doc for `cryptonote:construct_tx_and_get_tx_key` and its related functions

## [2017-07-24]
+ **Added**
    - document some types for RingCT in file `rctTypes.h`
+ **Removed**
    - `README.i18n.md`

## [2017-07-19]
+ **Added**
    - document the some tx-related class and functions
+ **Updated**
    - `README.md` specifying this project for reviewing the codebase
