// Copyright (c) 2014-2017, The Monero Project
// 
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification, are
// permitted provided that the following conditions are met:
// 
// 1. Redistributions of source code must retain the above copyright notice, this list of
//    conditions and the following disclaimer.
// 
// 2. Redistributions in binary form must reproduce the above copyright notice, this list
//    of conditions and the following disclaimer in the documentation and/or other
//    materials provided with the distribution.
// 
// 3. Neither the name of the copyright holder nor the names of its contributors may be
//    used to endorse or promote products derived from this software without specific
//    prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
// THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
// THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 
// Parts of this file are originally copyright (c) 2012-2013 The Cryptonote developers

#pragma once

#include <boost/variant.hpp>
#include <boost/functional/hash/hash.hpp>
#include <vector>
#include <cstring>  // memcmp
#include <sstream>
#include <atomic>
#include "serialization/serialization.h"
#include "serialization/variant.h"
#include "serialization/vector.h"
#include "serialization/binary_archive.h"
#include "serialization/json_archive.h"
#include "serialization/debug_archive.h"
#include "serialization/crypto.h"
#include "serialization/keyvalue_serialization.h" // eepe named serialization
#include "cryptonote_config.h"
#include "crypto/crypto.h"
#include "crypto/hash.h"
#include "misc_language.h"
#include "tx_extra.h"
#include "ringct/rctTypes.h"

namespace cryptonote {
    const static crypto::hash null_hash = AUTO_VAL_INIT(null_hash); ///< a 256-bit hash with all elements as 0
    const static crypto::hash8 null_hash8 = AUTO_VAL_INIT(null_hash8);  ///< a 64-bit hash with all elements as 0
    const static crypto::public_key null_pkey = AUTO_VAL_INIT(null_pkey);   /// a 256-bit key with elements as 0

    /**
     * @brief   define the ring signature as a set of signature
     * */
    typedef std::vector<crypto::signature> ring_signature;


    /* outputs */
    /**
     * @brief   output as a script
     * @note    to be implement
     * */
    struct txout_to_script {
        std::vector<crypto::public_key> keys;
        std::vector<uint8_t> script;

        BEGIN_SERIALIZE_OBJECT()
            FIELD(keys)
            FIELD(script)
        END_SERIALIZE()
    };

    /**
     * @brief   output as the hash of a script
     * @note    to be implement
     * */
    struct txout_to_scripthash {
        crypto::hash hash;
    };

    /**
     * @brief   output to a public key, i.e., an address
     * */
    struct txout_to_key {
        txout_to_key() {}

        txout_to_key(const crypto::public_key &_key) : key(_key) {}

        crypto::public_key key; ///< address of the payee, actually a public key whose secret key held by the payee
    };


    /* inputs */
    /**
     * @brief   input from a coinbase transaction
     * */
    struct txin_gen {
        size_t height;  ///< the block generating the coinbase transaction

        BEGIN_SERIALIZE_OBJECT()
            VARINT_FIELD(height)
        END_SERIALIZE()
    };

    /**
     * @brief   input as a script
     * @note    to be implement
     * */
    struct txin_to_script {
        crypto::hash prev;
        size_t prevout;
        std::vector<uint8_t> sigset;

        BEGIN_SERIALIZE_OBJECT()
            FIELD(prev)
            VARINT_FIELD(prevout)
            FIELD(sigset)
        END_SERIALIZE()
    };

    /**
     * @brief   input as the hash of a script
     * @note    to be implement
     * */
    struct txin_to_scripthash {
        crypto::hash prev;
        size_t prevout;
        txout_to_script script;
        std::vector<uint8_t> sigset;

        BEGIN_SERIALIZE_OBJECT()
            FIELD(prev)
            VARINT_FIELD(prevout)
            FIELD(script)
            FIELD(sigset)
        END_SERIALIZE()
    };

    /**
     * @brief   input as a redeemed output from previous transaction
     * @note    As for the `key_offsets`, key offsets are the set of outputs your ring is using as "fake"outputs,
     *              as well as yours. Outputs of a given denomination are ordered in blockchain order, and thus can be
     *              represented by their index in that list. This is smaller than using the public key. Moreover,
     *              they're stored as offsets from the previous one (the first one from 0), as this will result in
     *              smaller values, which can often result in a yet smaller amount of data, since those numbers are
     *              written out in a variable length output (kinda like UTF-8 in rough outline).
     * */
    struct txin_to_key {
        uint64_t amount;    ///< amount of the input coins
        std::vector<uint64_t> key_offsets;  ///< = the global out indices offset
                                        ///<  (only the 1st entry is the global output index),
                                        ///< including the mix-ins and the real one
        crypto::key_image k_image;      ///< key image related to the coin, for double spending protection

        BEGIN_SERIALIZE_OBJECT()
            VARINT_FIELD(amount)
            FIELD(key_offsets)
            FIELD(k_image)
        END_SERIALIZE()
    };


    /**
     * @brief   a variant of tx input, may be
     *              + `txin_gen`: coinbase tx
     *              + `txin_to_script`: a script
     *              + `txin_to_scripthash`: hash of a script
     *              + `txin_to_key`: a destination key
     * */
    typedef boost::variant<txin_gen, txin_to_script, txin_to_scripthash, txin_to_key> txin_v;

    /**
     * @brief   a variant of tx output, may be a script, hash of script, or a destination key
     * */
    typedef boost::variant<txout_to_script, txout_to_scripthash, txout_to_key> txout_target_v;

    //typedef std::pair<uint64_t, txout> out_t;
    /**
     * @brief   output entry of a transaction, containing an amount and a destination
     * */
    struct tx_out {
        uint64_t amount;    ///< amount of the output coin
        txout_target_v target;  ///< address of the payee, mostly are {@link txout_to_key}

        BEGIN_SERIALIZE_OBJECT()
            VARINT_FIELD(amount)
            FIELD(target)
        END_SERIALIZE()
    };

    /**
     * @brief   the prefix of a transaction
     * @details     The prefix consists of
     *                  + a version flag
     *                  + unlock_time
     *                  + a set of input coins
     *                  + a set of output coins
     *                  + a set of extra bytes
     * @note    `extra` field is a grab bag of metadata. Typically, it will include the public key R for that
     *           transaction's one time key. It can also include an "extra nonce", which is used for the payment id,
     *           if the transaction has one. There are other things that can be put in there, including user defined
     *           data if so wanted.
     * */
    class transaction_prefix {

    public:
        // tx information
        size_t version; ///< version of transaction prefix structure
        uint64_t unlock_time;  ///< number of block (or time), used as a limitation like: spend this tx not early then block/time

        std::vector<txin_v> vin;    ///< set of input coins
        std::vector<tx_out> vout;   ///< set of output coins
        //extra
        std::vector<uint8_t> extra; ///< reserved field for extra information

        BEGIN_SERIALIZE()
            VARINT_FIELD(version)
            if (version == 0 || CURRENT_TRANSACTION_VERSION < version) return false;
            VARINT_FIELD(unlock_time)
            FIELD(vin)
            FIELD(vout)
            FIELD(extra)
        END_SERIALIZE()

    public:
        /**
         * @brief   default constructor doing nothing
         * */
        transaction_prefix() {}
    };

    /**
     * @brief   a wrapper for a transaction set
     * */
    class transaction : public transaction_prefix {
    private:
        // hash cash
        mutable std::atomic<bool> hash_valid;   ///< flag indicating whether txs in valid
        mutable std::atomic<bool> blob_size_valid;  ///< flag indicating if the blob serialized from txs is valid

    public:
        std::vector<std::vector<crypto::signature> > signatures; ///< matrix signatures matrix,  always the same as inputs count
        rct::rctSig rct_signatures; ///< RCT signature for current tx set

        // hash cash
        mutable crypto::hash hash;
        mutable size_t blob_size;   ///< size of the blob in bytes

        /**
         * @brief   default constructor doing nothing
         * */
        transaction();

        /**
         * @brief   copy constructor
         * @param t the source constructor to copy
         * */
        transaction(const transaction &t) : transaction_prefix(t), hash_valid(false), blob_size_valid(false),
                                            signatures(t.signatures), rct_signatures(t.rct_signatures) {
            if (t.is_hash_valid()) {
                hash = t.hash;
                set_hash_valid(true);
            }
            if (t.is_blob_size_valid()) {
                blob_size = t.blob_size;
                set_blob_size_valid(true);
            }
        }

        /**
         * @brief   overloaded assignment operator for copying
         * @param t the source constructor to assign
         * */
        transaction &operator=(const transaction &t) {
            transaction_prefix::operator=(t);
            set_hash_valid(false);
            set_blob_size_valid(false);
            signatures = t.signatures;
            rct_signatures = t.rct_signatures;
            if (t.is_hash_valid()) {
                hash = t.hash;
                set_hash_valid(true);
            }
            if (t.is_blob_size_valid()) {
                blob_size = t.blob_size;
                set_blob_size_valid(true);
            }
            return *this;
        }

        virtual ~transaction();

        void set_null();

        void invalidate_hashes();

        /**
         * @brief   check if the hash of txs is valid
         * @return  `true` in case of valid hash, and `false` otherwise
         * */
        bool is_hash_valid() const { return hash_valid.load(std::memory_order_acquire); }

        /**
         * @brief   update the flag indicating the validity of txs' hash
         * @param v a boolean to update the flag for validity of the txs' hash
         * */
        void set_hash_valid(bool v) const { hash_valid.store(v, std::memory_order_release); }

        /**
         * @brief   check if the blob size of txs is valid
         * @return  `true` in case of valid size, and `false` otherwise
         * */
        bool is_blob_size_valid() const { return blob_size_valid.load(std::memory_order_acquire); }

        /**
         * @brief   update the flag indicating the validity of txs' size
         * @param v a boolean to update the flag for validity of the txs' size
         * */
        void set_blob_size_valid(bool v) const { blob_size_valid.store(v, std::memory_order_release); }

        BEGIN_SERIALIZE_OBJECT()
            if (!typename Archive<W>::is_saving()) {
                set_hash_valid(false);
                set_blob_size_valid(false);
            }

            FIELDS(*static_cast<transaction_prefix *>(this))

            if (version == 1) {
                ar.tag("signatures");
                ar.begin_array();
                PREPARE_CUSTOM_VECTOR_SERIALIZATION(vin.size(), signatures);
                bool signatures_not_expected = signatures.empty();
                if (!signatures_not_expected && vin.size() != signatures.size())
                    return false;

                for (size_t i = 0; i < vin.size(); ++i) {
                    size_t signature_size = get_signature_size(vin[i]);
                    if (signatures_not_expected) {
                        if (0 == signature_size)
                            continue;
                        else
                            return false;
                    }

                    PREPARE_CUSTOM_VECTOR_SERIALIZATION(signature_size, signatures[i]);
                    if (signature_size != signatures[i].size())
                        return false;

                    FIELDS(signatures[i]);

                    if (vin.size() - i > 1)
                        ar.delimit_array();
                }
                ar.end_array();
            } else {
                ar.tag("rct_signatures");
                if (!vin.empty()) {
                    ar.begin_object();
                    bool r = rct_signatures.serialize_rctsig_base(ar, vin.size(), vout.size());
                    if (!r || !ar.stream().good()) return false;
                    ar.end_object();
                    if (rct_signatures.type != rct::RCTTypeNull) {
                        ar.tag("rctsig_prunable");
                        ar.begin_object();
                        r = rct_signatures.p.serialize_rctsig_prunable(ar, rct_signatures.type, vin.size(), vout.size(),
                                                                       vin[0].type() == typeid(txin_to_key) ?
                                                                       boost::get<txin_to_key>(
                                                                               vin[0]).key_offsets.size() - 1 : 0);
                        if (!r || !ar.stream().good()) return false;
                        ar.end_object();
                    }
                }
            }
        END_SERIALIZE()

        template<bool W, template<bool> class Archive>
        bool serialize_base(Archive<W> &ar) {
            FIELDS(*static_cast<transaction_prefix *>(this))

            if (version == 1) {
            } else {
                ar.tag("rct_signatures");
                if (!vin.empty()) {
                    ar.begin_object();
                    bool r = rct_signatures.serialize_rctsig_base(ar, vin.size(), vout.size());
                    if (!r || !ar.stream().good()) return false;
                    ar.end_object();
                }
            }
            return true;
        }

    private:
        static size_t get_signature_size(const txin_v &tx_in);
    };


    /**
     * @brief   default constructor
     * */
    inline
    transaction::transaction() {
        set_null();
    }

    /**
     * @brief   virtual destructor dealing with resources releasing
     * */
    inline transaction::~transaction() {
        //set_null();
    }

    /**
     * @brief   set all values of txs to empty
     * @details version flag as 1, unlock time as 0, empty set of input/output coins, extra fields,
     *          signatures, RCT signature, invalid hash and size flag
     * */
    inline void transaction::set_null() {
        version = 1;
        unlock_time = 0;
        vin.clear();
        vout.clear();
        extra.clear();
        signatures.clear();
        rct_signatures.type = rct::RCTTypeNull;
        set_hash_valid(false);
        set_blob_size_valid(false);
    }

    /**
     * @brief   mark the hash of txs as invalid
     * @details     also update the size of txs as invalid
     * */
    inline void transaction::invalidate_hashes() {
        set_hash_valid(false);
        set_blob_size_valid(false);
    }

    /**
     * @brief   estimate the necessary size of the signature for a tx
     * @param tx_in     the tx containing inputs for signing
     * */
    inline size_t transaction::get_signature_size(const txin_v &tx_in) {
        struct txin_signature_size_visitor : public boost::static_visitor<size_t> {
            size_t operator()(const txin_gen &txin) const { return 0; }

            size_t operator()(const txin_to_script &txin) const { return 0; }

            size_t operator()(const txin_to_scripthash &txin) const { return 0; }

            size_t operator()(const txin_to_key &txin) const { return txin.key_offsets.size(); }
        };

        return boost::apply_visitor(txin_signature_size_visitor(), tx_in);
    }



    /************************************************************************/
    /*                                                                      */
    /************************************************************************/
    /**
     * @brief     container for the meta information of a block
     * @details   contains 2 version flags (a major, a minor), a timestamp, hash of previous block,
     *            a nonce solving the PoW puzzle
     * */
    struct block_header {
        uint8_t major_version;  ///< major version flag for the header
        uint8_t minor_version;  // now used as a voting mechanism, rather than how this particular block is built
        uint64_t timestamp; ///< timestamp recoding the moment when the block is created
        crypto::hash prev_id;  ///< hash of the previous block
        uint32_t nonce; ///< nonce proving the PoW

        BEGIN_SERIALIZE()
            VARINT_FIELD(major_version)
            VARINT_FIELD(minor_version)
            VARINT_FIELD(timestamp)
            FIELD(prev_id)
            FIELD(nonce)
        END_SERIALIZE()
    };

    /**
     * @brief   wrapper for a block in the chain
     * */
    struct block : public block_header {
    private:
        // hash cash
        mutable std::atomic<bool> hash_valid; ///< boolean indicating whether the hash of current is valid

    public:
        /**
         * @brief   default constructor initializing the block with a empty header and invalid hash flag
         * */
        block() : block_header(), hash_valid(false) {}

        /**
         * @brief   copy constructor initializing this block with a reference block
         * */
        block(const block &b) : block_header(b), hash_valid(false), miner_tx(b.miner_tx), tx_hashes(b.tx_hashes) {
            if (b.is_hash_valid()) {
                hash = b.hash;
                set_hash_valid(true);
            }
        }

        /**
         * @brief   an overloaded assignment operator for the block
         * @param b the reference block to copy from
         * @return  the reference of this block with all values updated accordingly
         * */
        block &operator=(const block &b) {
            block_header::operator=(b);
            hash_valid = false;
            miner_tx = b.miner_tx;
            tx_hashes = b.tx_hashes;
            if (b.is_hash_valid()) {
                hash = b.hash;
                set_hash_valid(true);
            }
            return *this;
        }

        /**
         * @brief   set the underlying `hash_valid` flag to false
         * */
        void invalidate_hashes() { set_hash_valid(false); }

        /**
         * @brief   chech if the hash of current block is valid, i.e., the block should have been mined
         * @return  `true` in case of valid hash, and `false` otherwise
         * */
        bool is_hash_valid() const { return hash_valid.load(std::memory_order_acquire); }

        /**
         * @brief   update the flag indicating whether the hash of current block is valid
         * @param v boolean to update the indicating flag
         * */
        void set_hash_valid(bool v) const { hash_valid.store(v, std::memory_order_release); }

        transaction miner_tx;   ///< transaction collected by the miner
        std::vector<crypto::hash> tx_hashes;    ///< a set of 257-bit hashes (? 1-to-1 to `miner_tx`)

        // hash cash
        mutable crypto::hash hash; ///< what for

        BEGIN_SERIALIZE_OBJECT()
            if (!typename Archive<W>::is_saving())
                set_hash_valid(false);

            FIELDS(*static_cast<block_header *>(this))
            FIELD(miner_tx)
            FIELD(tx_hashes)
        END_SERIALIZE()
    };


    /************************************************************************/
    /*                                                                      */
    /************************************************************************/
    /**
     * @brief   container for public keys related to the address,
     *          including a public spending key, and a public viewing key
     * */
    struct account_public_address {
        crypto::public_key m_spend_public_key;  ///< public key of the spending key pair
        crypto::public_key m_view_public_key;   ///< public key for the viewing key pair

        BEGIN_SERIALIZE_OBJECT()
            FIELD(m_spend_public_key)
            FIELD(m_view_public_key)
        END_SERIALIZE()

    BEGIN_KV_SERIALIZE_MAP()
            KV_SERIALIZE_VAL_POD_AS_BLOB_FORCE(m_spend_public_key)
            KV_SERIALIZE_VAL_POD_AS_BLOB_FORCE(m_view_public_key)
        END_KV_SERIALIZE_MAP()
    };

    /**
     * @brief   a container for sk-pk pair
     * */
    struct keypair {
        crypto::public_key pub; ///< a point on EC, as a public key
        crypto::secret_key sec; ///< a scalar on EC group, as a secret key

        /**
         * @brief   generate a sk-pk pair randomly
         * @return a generate sk-pk pair
         * */
        static inline keypair generate() {
            keypair k;
            generate_keys(k.pub, k.sec);
            return k;
        }
    };
    //---------------------------------------------------------------

}

BLOB_SERIALIZER(cryptonote::txout_to_key);
BLOB_SERIALIZER(cryptonote::txout_to_scripthash);

VARIANT_TAG(binary_archive, cryptonote::txin_gen, 0xff);

VARIANT_TAG(binary_archive, cryptonote::txin_to_script, 0x0);

VARIANT_TAG(binary_archive, cryptonote::txin_to_scripthash, 0x1);

VARIANT_TAG(binary_archive, cryptonote::txin_to_key, 0x2);

VARIANT_TAG(binary_archive, cryptonote::txout_to_script, 0x0);

VARIANT_TAG(binary_archive, cryptonote::txout_to_scripthash, 0x1);

VARIANT_TAG(binary_archive, cryptonote::txout_to_key, 0x2);

VARIANT_TAG(binary_archive, cryptonote::transaction, 0xcc);

VARIANT_TAG(binary_archive, cryptonote::block, 0xbb);

VARIANT_TAG(json_archive, cryptonote::txin_gen, "gen");

VARIANT_TAG(json_archive, cryptonote::txin_to_script, "script");

VARIANT_TAG(json_archive, cryptonote::txin_to_scripthash, "scripthash");

VARIANT_TAG(json_archive, cryptonote::txin_to_key, "key");

VARIANT_TAG(json_archive, cryptonote::txout_to_script, "script");

VARIANT_TAG(json_archive, cryptonote::txout_to_scripthash, "scripthash");

VARIANT_TAG(json_archive, cryptonote::txout_to_key, "key");

VARIANT_TAG(json_archive, cryptonote::transaction, "tx");

VARIANT_TAG(json_archive, cryptonote::block, "block");

VARIANT_TAG(debug_archive, cryptonote::txin_gen, "gen");

VARIANT_TAG(debug_archive, cryptonote::txin_to_script, "script");

VARIANT_TAG(debug_archive, cryptonote::txin_to_scripthash, "scripthash");

VARIANT_TAG(debug_archive, cryptonote::txin_to_key, "key");

VARIANT_TAG(debug_archive, cryptonote::txout_to_script, "script");

VARIANT_TAG(debug_archive, cryptonote::txout_to_scripthash, "scripthash");

VARIANT_TAG(debug_archive, cryptonote::txout_to_key, "key");

VARIANT_TAG(debug_archive, cryptonote::transaction, "tx");

VARIANT_TAG(debug_archive, cryptonote::block, "block");
