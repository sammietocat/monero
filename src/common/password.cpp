// Copyright (c) 2014-2017, The Monero Project
// 
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification, are
// permitted provided that the following conditions are met:
// 
// 1. Redistributions of source code must retain the above copyright notice, this list of
//    conditions and the following disclaimer.
// 
// 2. Redistributions in binary form must reproduce the above copyright notice, this list
//    of conditions and the following disclaimer in the documentation and/or other
//    materials provided with the distribution.
// 
// 3. Neither the name of the copyright holder nor the names of its contributors may be
//    used to endorse or promote products derived from this software without specific
//    prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
// THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
// THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 
// Parts of this file are originally copyright (c) 2012-2013 The Cryptonote developers

/**
 * @file    password.cpp
 * */

#include "password.h"

#include <iostream>
#include <memory.h>
#include <stdio.h>

#if defined(_WIN32)
#include <io.h>
#include <windows.h>
#else

#include <termios.h>
#include <unistd.h>

#endif

#ifdef HAVE_READLINE
#include "readline_buffer.h"
#endif

namespace {
#if defined(_WIN32)
    bool is_cin_tty() noexcept
    {
      return 0 != _isatty(_fileno(stdin));
    }

    bool read_from_tty(std::string& pass)
    {
      static constexpr const char BACKSPACE = 8;

      HANDLE h_cin = ::GetStdHandle(STD_INPUT_HANDLE);

      DWORD mode_old;
      ::GetConsoleMode(h_cin, &mode_old);
      DWORD mode_new = mode_old & ~(ENABLE_ECHO_INPUT | ENABLE_LINE_INPUT);
      ::SetConsoleMode(h_cin, mode_new);

      bool r = true;
      pass.reserve(tools::password_container::max_password_size);
      while (pass.size() < tools::password_container::max_password_size)
      {
        DWORD read;
        char ch;
        r = (TRUE == ::ReadConsoleA(h_cin, &ch, 1, &read, NULL));
        r &= (1 == read);
        if (!r)
        {
          break;
        }
        else if (ch == '\n' || ch == '\r')
        {
          std::cout << std::endl;
          break;
        }
        else if (ch == BACKSPACE)
        {
          if (!pass.empty())
          {
            pass.back() = '\0';
            pass.resize(pass.size() - 1);
            std::cout << "\b \b";
          }
        }
        else
        {
          pass.push_back(ch);
          std::cout << '*';
        }
      }

      ::SetConsoleMode(h_cin, mode_old);

      return r;
    }

#else // end WIN32 

    /**
     * @brief check if the stdin is a TTY terminal (i.e., a console capable of accepting inputs)
     * @return  `true` if the stdin is a TTY, and `false` otherwise
     * */
    bool is_cin_tty() noexcept {
        return 0 != isatty(fileno(stdin));
    }

    int getch() noexcept {
        struct termios tty_old;
        tcgetattr(STDIN_FILENO, &tty_old);

        struct termios tty_new;
        tty_new = tty_old;
        tty_new.c_lflag &= ~(ICANON | ECHO);
        tcsetattr(STDIN_FILENO, TCSANOW, &tty_new);

        int ch = getchar();

        tcsetattr(STDIN_FILENO, TCSANOW, &tty_old);

        return ch;
    }

    /**
     * @brief   read a password string from the stdin
     * @details     During reading, EOF will abort the operation and treat it as a failure.
     *              `\n` and `\r` signals an end of reading.
     *              Backspace will remove last character read in.
     *              A `*` represents a character read in.
     * @param aPass     buffer to store the string read out
     * @return  `true` in case of successful reading, and `false` otherwise
     * */
    bool read_from_tty(std::string &aPass) {
        static constexpr const char BACKSPACE = 127;

        // reserve space
        aPass.reserve(tools::password_container::max_password_size);
        while (aPass.size() < tools::password_container::max_password_size) {
            int ch = getch();
            if (EOF == ch) {
                return false;
            } else if (ch == '\n' || ch == '\r') {
                std::cout << std::endl;
                break;
            } else if (ch == BACKSPACE) {
                if (!aPass.empty()) {
                    aPass.back() = '\0';
                    aPass.resize(aPass.size() - 1);
                    std::cout << "\b \b";
                }
            } else {
                aPass.push_back(ch);
                std::cout << '*';
            }
        }

        return true;
    }

#endif // end !WIN32

    /**
     * @brief   replace all characters in a string with '\0' and then remove them
     * @param pass  password string to clear
     * */
    void clear(std::string &pass) noexcept {
        //! TODO Call a memory wipe function that hopefully is not optimized out
        pass.replace(0, pass.capacity(), pass.capacity(), '\0');
        pass.clear();
    }

    /**
     * @brief read 2 password from the stdin in a TTY
     * @param verify    switch controlling whether verify the equality of the 2 password string read out
     * @param message   string prompting inputs
     * @param pass1     password container to store the 1st password string read out
     * @param pass2     password container to store the 2nd password string read out
     * */
    bool read_from_tty(const bool verify, const char *message, std::string &pass1, std::string &pass2) {
        while (true) {
            if (message)
                std::cout << message << ": ";
            if (!read_from_tty(pass1))
                return false;
            if (verify) {
                std::cout << "Confirm Password: ";
                if (!read_from_tty(pass2))
                    return false;
                if (pass1 != pass2) {
                    std::cout << "Passwords do not match! Please try again." << std::endl;
                    clear(pass1);
                    clear(pass2);
                } else //new password matches
                    return true;
            } else
                return true;
            //No need to verify password entered at this point in the code
        }

        return false;
    }

    /**
     * @brief   read a password from the file bound to the stdin
     * @details     `EOF`, `\n` and `\r` means the end of reading
     * @param pass  buffer to hold up the password read out
     * @return  `true` in case of no errors/failures
     * */
    bool read_from_file(std::string &pass) {
        pass.reserve(tools::password_container::max_password_size);
        for (size_t i = 0; i < tools::password_container::max_password_size; ++i) {
            char ch = static_cast<char>(std::cin.get());
            if (std::cin.eof() || ch == '\n' || ch == '\r') {
                break;
            } else if (std::cin.fail()) {
                return false;
            } else {
                pass.push_back(ch);
            }
        }
        return true;
    }

} // anonymous namespace

namespace tools {
    // deleted via private member
    /**
     * @brief   initialize as a empty password
     * */
    password_container::password_container() noexcept : m_password() {}

    /**
     * @brief   a move constructor
     * @details     the password argument passed in will be empty,
     *              whose underlying password string will be moved to this object
     * */
    password_container::password_container(std::string &&password) noexcept
            : m_password(std::move(password)) {
    }

    /**
     * @brief   empty the container
     * @details set the underlying password string to an empty string
     * */
    password_container::~password_container() noexcept {
        clear(m_password);
    }

    /**
     * @brief   read in a password
     * @param verify    switch indicating confirmation of the password entered
     * @param message   prompt to display when asking input
     * @return  a optional container containing the password enter, which will be `boost::none` in case of error
     * */
    boost::optional<password_container> password_container::prompt(const bool verify, const char *message) {
#ifdef HAVE_READLINE
        rdln::suspend_readline pause_readline;
#endif
        password_container pass1{};
        password_container pass2{};
        // read from the stdin bound with a TTY or file
        if (is_cin_tty() ? read_from_tty(verify, message, pass1.m_password, pass2.m_password) : read_from_file(
                pass1.m_password))
            return {std::move(pass1)};

        return boost::none;
    }

    /**
     * @brief   Extracts username and password from the format `username:password`.
     * @details     A blank username or password is allowed. If the `:` character is not present,
     *              `password_container::prompt` will be called by forwarding the `verify` and `message` arguments.
     * @param userpass Is "consumed", and the memory contents are wiped.
     * @param verify is passed to `password_container::prompt` if necessary.
     * @param message is passed to `password_container::prompt` if necessary.
     * @return The username and password, or boost::none if `password_container::prompt` fails.
     * @note    If the password field is missing in `userpass`, the user will be requested for entering a password
     *          from the stdin
     */
    boost::optional<login> login::parse(std::string &&userpass, bool verify, const char *message) {
        login out{};
        password_container wipe{std::move(userpass)};

        const auto loc = wipe.password().find(':');
        if (loc == std::string::npos) {
            auto result = tools::password_container::prompt(verify, message);
            if (!result)
                return boost::none;

            out.password = std::move(*result);
        } else {
            out.password = password_container{wipe.password().substr(loc + 1)};
        }

        out.username = wipe.password().substr(0, loc);
        return {std::move(out)};
    }
} // namespace tools
