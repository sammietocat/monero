// Copyright (c) 2014-2017, The Monero Project
// 
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification, are
// permitted provided that the following conditions are met:
// 
// 1. Redistributions of source code must retain the above copyright notice, this list of
//    conditions and the following disclaimer.
// 
// 2. Redistributions in binary form must reproduce the above copyright notice, this list
//    of conditions and the following disclaimer in the documentation and/or other
//    materials provided with the distribution.
// 
// 3. Neither the name of the copyright holder nor the names of its contributors may be
//    used to endorse or promote products derived from this software without specific
//    prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
// THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
// THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 
// Parts of this file are originally copyright (c) 2012-2013 The Cryptonote developers

/**
 * @file    password.h
 * @brief   a customized container for password of max length 1024 byte
 * */
#pragma once

#include <string>
#include <boost/optional/optional.hpp>

namespace tools
{
    /**
     * @brief   a customized container for password of max length 1024 byte
     * */
  class password_container
  {
  public:
    static constexpr const size_t max_password_size = 1024; ///< max length of the password string

    //! Empty password
    password_container() noexcept;

    //! `password` is used as password
    password_container(std::string&& password) noexcept;

    //! \return A password from stdin TTY prompt or `std::cin` pipe.
    static boost::optional<password_container> prompt(bool verify, const char *mesage = "Password");

      /**
       * @brief     remove the copy constructor
       * */
    password_container(const password_container&) = delete;
      /**
       * @brief     a move constructor built by the compiler
       * @param rhs source password container to move
       * */
    password_container(password_container&& rhs) = default;

    //! Wipes internal password
    ~password_container() noexcept;

    password_container& operator=(const password_container&) = delete;
      /**
       * @brief     a move assignment built by the compiler
       * */
    password_container& operator=(password_container&&) = default;

      /**
       * @brief     get the password contained
       * @return    the underlying password string
       * */
    const std::string& password() const noexcept { return m_password; }

  private:
    //! TODO Custom allocator that locks to RAM?
    std::string m_password; ///< password string
  };

    /**
     * @brief a container storing a (username,password) pair for login authentication
     * */
  struct login
  {
      /**
       * @brief default constructor to be built by the compiler
       * */
    login() = default;

    /**
     * @brief   Extracts username and password from the format `username:password`.
     * @details     A blank username or password is allowed. If the `:` character is not present,
     *              `password_container::prompt` will be called by forwarding the `verify` and `message` arguments.
     * @param userpass Is "consumed", and the memory contents are wiped.
     * @param verify is passed to `password_container::prompt` if necessary.
     * @param message is passed to `password_container::prompt` if necessary.
     * @return The username and password, or boost::none if `password_container::prompt` fails.
     */
    static boost::optional<login> parse(std::string&& userpass, bool verify, const char* message = "Password");

      /**
       * @brief     remove the copy constructor
       * */
    login(const login&) = delete;
      /**
       * @brief ask the compiler to make a move constructor for us
       * */
    login(login&&) = default;
      /**
       * @brief ask the compiler to make destructor for us
       * */
    ~login() = default;
      /**
       * @brief remove the copy assignment operator
       * */
    login& operator=(const login&) = delete;
      /**
       * @brief     ask the compiler to make a move assignment operator for us
       * */
    login& operator=(login&&) = default;

    std::string username; ///< user name for login
    password_container password;  ///< password wrapper for login
  };
}
