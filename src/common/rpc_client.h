// Copyright (c) 2014-2017, The Monero Project
// 
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification, are
// permitted provided that the following conditions are met:
// 
// 1. Redistributions of source code must retain the above copyright notice, this list of
//    conditions and the following disclaimer.
// 
// 2. Redistributions in binary form must reproduce the above copyright notice, this list
//    of conditions and the following disclaimer in the documentation and/or other
//    materials provided with the distribution.
// 
// 3. Neither the name of the copyright holder nor the names of its contributors may be
//    used to endorse or promote products derived from this software without specific
//    prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
// THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
// THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

/**
 * @file    rpc_client.h
 * @brief   implementation of RPC client
 * */
#pragma once

#include <boost/optional/optional.hpp>

#include "common/http_connection.h"
#include "common/scoped_message_writer.h"
#include "rpc/core_rpc_server_commands_defs.h"
#include "storages/http_abstract_invoke.h"
#include "net/http_auth.h"
#include "net/http_client.h"
#include "string_tools.h"

namespace tools {
    /**
     * @brief   a wrapper of http client handling RPC calling
     * @details     the actual job is delegated to the embedded {@link epee::net_utils::http::http_simple_client}
     * */
    class t_rpc_client final {
    private:
        epee::net_utils::http::http_simple_client m_http_client;    ///< proxy doing the heavy lifting job
    public:
        /**
         * @brief    construct a client handling the connection to a given server address at the given (username,password)
         * @param ip  IP of the server
         * @param port    port listened by the server to handle request
         * @param user    container of a (username,password) pair for authentication
         * */
        t_rpc_client(
                uint32_t ip, uint16_t port, boost::optional<epee::net_utils::http::login> user
        )
                : m_http_client{} {
            m_http_client.set_server(
                    epee::string_tools::get_ip_string_from_int32(ip), std::to_string(port), std::move(user)
            );
        }

        /**
         * @brief     do a RPC request with data in json on api "/json_rpc"
         * @param req request container with query parameters
         * @param res response container with message feedback
         * @param method_name name of the RPC method to call
         * @return    `true` in case of successful handling regardless of the status of the response,
         *              and `false` otherwise
         * */
        template<typename T_req, typename T_res>
        bool basic_json_rpc_request(
                T_req &req, T_res &res, std::string const &method_name
        ) {
            t_http_connection connection(&m_http_client);

            bool ok = connection.is_open();
            if (!ok) {
                fail_msg_writer() << "Couldn't connect to daemon";
                return false;
            }
            ok = ok && epee::net_utils::invoke_http_json_rpc("/json_rpc", method_name, req, res, m_http_client,
                                                             t_http_connection::TIMEOUT());
            if (!ok) {
                fail_msg_writer() << "Daemon request failed";
                return false;
            } else {
                return true;
            }
        }

        /**
         * @brief     make a RPC request in json to invoke a specified method in the server side
         * @param req request wrapper of query parameters
         * @param res response wrapper of feedback message
         * @param method_name name of RPC method to invoke
         * @param fail_msg    message to print in case of the response with NOT OK status
         * @return    `true` if the response is with OK status, and `false` otherwise
         * */
        template<typename T_req, typename T_res>
        bool json_rpc_request(
                T_req &req, T_res &res, std::string const &method_name, std::string const &fail_msg
        ) {
            t_http_connection connection(&m_http_client);

            bool ok = connection.is_open();
            ok = ok && epee::net_utils::invoke_http_json_rpc("/json_rpc", method_name, req, res, m_http_client,
                                                             t_http_connection::TIMEOUT());
            if (!ok) {
                fail_msg_writer() << "Couldn't connect to daemon";
                return false;
            } else if (res.status != CORE_RPC_STATUS_OK) // TODO - handle CORE_RPC_STATUS_BUSY ?
            {
                fail_msg_writer() << fail_msg << " -- " << res.status;
                return false;
            } else {
                return true;
            }
        }

        /**
         * @brief     make a RPC request in json to invoke a method at a relative url in the server side
         * @param relative_url a relative url to handle incoming request
         * @param req request wrapper of query parameters
         * @param res response wrapper of feedback message
         * @param method_name name of RPC method to invoke
         * @param fail_msg    message to print in case of the response with NOT OK status
         * @return    `true` if the response is with OK status, and `false` otherwise
         * */
        template<typename T_req, typename T_res>
        bool rpc_request(
                T_req &req, T_res &res, std::string const &relative_url, std::string const &fail_msg
        ) {
            t_http_connection connection(&m_http_client);

            bool ok = connection.is_open();
            ok = ok &&
                 epee::net_utils::invoke_http_json(relative_url, req, res, m_http_client, t_http_connection::TIMEOUT());
            if (!ok) {
                fail_msg_writer() << "Couldn't connect to daemon";
                return false;
            } else if (res.status != CORE_RPC_STATUS_OK) // TODO - handle CORE_RPC_STATUS_BUSY ?
            {
                fail_msg_writer() << fail_msg << " -- " << res.status;
                return false;
            } else {
                return true;
            }
        }

        /**
         * @brief check if the client can reach the server
         * @return `true` if yes, and `false` otherwise
         * */
        bool check_connection() {
            t_http_connection connection(&m_http_client);
            return connection.is_open();
        }
    };
}
