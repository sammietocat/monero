var searchData=
[
  ['unspent',['UNSPENT',['../structcryptonote_1_1COMMAND__RPC__IS__KEY__IMAGE__SPENT.html#aeccb2dccaeb5c2e3c60e13bda6dd912aa58bae5247f2a685e9432c75c471e1faf',1,'cryptonote::COMMAND_RPC_IS_KEY_IMAGE_SPENT']]],
  ['updateneeded',['UpdateNeeded',['../classcryptonote_1_1HardFork.html#a0272dfdf8336987d3e94f611a86cb67cae9b49bb12b2fcf3d4a208a09a189690c',1,'cryptonote::HardFork']]],
  ['updates_5fdisabled',['UPDATES_DISABLED',['../classcryptonote_1_1core.html#a9868015010478758e88ace172659bdaaafbb110827558d1f6e198f2a7c02d53c4',1,'cryptonote::core']]],
  ['updates_5fdownload',['UPDATES_DOWNLOAD',['../classcryptonote_1_1core.html#a9868015010478758e88ace172659bdaaada18c76a4cb560bc1cc407c498eb07b8',1,'cryptonote::core']]],
  ['updates_5fnotify',['UPDATES_NOTIFY',['../classcryptonote_1_1core.html#a9868015010478758e88ace172659bdaaa676a8fcc6656f25c44a35ba243ef727f',1,'cryptonote::core']]],
  ['updates_5fupdate',['UPDATES_UPDATE',['../classcryptonote_1_1core.html#a9868015010478758e88ace172659bdaaaa86bf7564fa76bbae7d5933d8f91c1ea',1,'cryptonote::core']]]
];
