var searchData=
[
  ['unconfirmed_5ftransfer_5fdetails',['unconfirmed_transfer_details',['../structtools_1_1wallet2_1_1unconfirmed__transfer__details.html',1,'tools::wallet2']]],
  ['unexpected_5ftxin_5ftype',['unexpected_txin_type',['../structtools_1_1error_1_1unexpected__txin__type.html',1,'tools::error']]],
  ['unsigned_5ftx_5fset',['unsigned_tx_set',['../structtools_1_1wallet2_1_1unsigned__tx__set.html',1,'tools::wallet2']]],
  ['unsignedtransaction',['UnsignedTransaction',['../structMonero_1_1UnsignedTransaction.html',1,'Monero']]],
  ['unsignedtransactionimpl',['UnsignedTransactionImpl',['../classMonero_1_1UnsignedTransactionImpl.html',1,'Monero']]],
  ['uri_5fspec',['uri_spec',['../structtools_1_1wallet__rpc_1_1uri__spec.html',1,'tools::wallet_rpc']]]
];
