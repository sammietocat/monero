#pragma once

#include <iostream>

#include "cryptonote_core/cryptonote_tx_utils.h" // from cryptonote_core
#include "ringct/rctTypes.h"    // from ringct
#include "hex.h"

namespace sammy {
    namespace fmt {
        using namespace std;

        void println(const rct::ctkey &k);

        void println(const cryptonote::tx_source_entry &src);

        void println(const cryptonote::tx_destination_entry &dest);

        string to_string(const rct::key& k);
    }
}