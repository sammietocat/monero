/**
 * @file    rctplusOps.hpp
 * @brief   set of functions to test RCT related functions in monero
 * */
#include <numeric>
#include <vector>

#include "include_base_utils.h" //from epee
#include "cryptonote_basic/account.h"   // from cryptonote_basic
#include "cryptonote_core/cryptonote_tx_utils.h"    // from cryptonote_core
#include "ringct/rctOps.h"  // from ringct
#include "ringct/rctSigs.h" //from ringct
#include "string_tools.h" // from epee

namespace sammy {
    namespace rctplus {

        bool isPaired(const rct::key &sk, const rct::key &pk);

        bool isPaired(const rct::ctkey &ctsk, const rct::ctkey &ctpk, const rct::xmr_amount &amt);

        tuple<rct::ctkeyV, rct::ctkeyV> ctskpkVecGen(const vector<rct::xmr_amount> &amtVec);

        rct::keyV pkVecGen(const int nPk);

        vector<rct::xmr_amount> randomXmrAmountVector(const int nSrc, const rct::xmr_amount amtSum = 0);

        bool fakeWalletSource(const cryptonote::account_keys accountKeys, const crypto::public_key txPk,
                              const size_t realOutputIdx, const rct::xmr_amount amt, rct::ctkey &inSk,
                              rct::ctkey &inPk);

        vector<cryptonote::tx_source_entry>
        randomTxSourcesSimple(const cryptonote::account_keys &accountKeys, const int nSrc, const rct::xmr_amount amtSum,
                              const int mixinCount, rct::ctkeyV &inSkVec);

        vector<cryptonote::tx_source_entry>
        randomTxSourcesFull(const cryptonote::account_keys &accountKeys, const int nSrc, const rct::xmr_amount amtSum,
                            const int mixinCount, rct::ctkeyV &inSkVec);

        vector<cryptonote::tx_destination_entry> randomDestinations(const int nDsts, rct::xmr_amount amtSum);

        bool randomRCTSimple(const int nSrc, const rct::xmr_amount amtSum, const int mixinCount, const int nDsts,
                             cryptonote::account_base &sender, cryptonote::transaction &tx,
                             crypto::secret_key &txSecKey, const rct::xmr_amount fee = 16);

        bool randomRCTFull(const int nSrc, const rct::xmr_amount amtSum, const int mixinCount, const int nDsts,
                           cryptonote::account_base &sender, cryptonote::transaction &tx,
                           crypto::secret_key &txSecKey, const rct::xmr_amount fee = 16);

        bool constructRCTAndGetTxKey(const cryptonote::account_keys &sender_account_keys,
                                     const std::vector<cryptonote::tx_source_entry> &sources,
                                     const std::vector<cryptonote::tx_destination_entry> &destinations,
                                     const std::vector<uint8_t> extra, const uint8_t rctType,
                                     cryptonote::transaction &tx, uint64_t unlock_time, crypto::secret_key &tx_key);
    }
}
