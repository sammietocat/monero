#include "fmt.hpp"

namespace sammy {
    namespace fmt {
        using namespace std;

        void println(const rct::ctkey &k) {
            cout << "(dest = ";
            epee::to_hex::buffer(std::cout, k.dest.bytes);
            cout << ",\n mask = ";
            epee::to_hex::buffer(std::cout, k.mask.bytes);
            cout << ")" << endl;
        }

        void println(const cryptonote::tx_source_entry &src) {
            std::string indices;
            std::for_each(src.outputs.begin(), src.outputs.end(),
                          [&](const cryptonote::tx_source_entry::output_entry& s_e) {
                              indices += std::to_string(s_e.first) + " ";
                          });
            cout << "amount=" << cryptonote::print_money(src.amount) << ", real_output=" << src.real_output
                 << ", real_output_in_tx_index=" << src.real_output_in_tx_index << ", indexes: " << indices << endl;
        }

        /**
         * @brief print a given tx destination entry
         * @param dest the destination to print
         * */
        void println(const cryptonote::tx_destination_entry &dest) {
            cout << "(      amount = "<<dest.amount<<","<<endl;
            cout << "  view-pubkey = ";
            epee::to_hex::buffer(std::cout,rct::pk2rct(dest.addr.m_view_public_key).bytes);
            cout<<",\n spend-pubkey = ";
            epee::to_hex::buffer(std::cout,rct::pk2rct(dest.addr.m_spend_public_key).bytes);
            cout<<")"<<endl;
        }

        string to_string(const rct::key& k) {
            return epee::to_hex::string(k.bytes);
        }
    }
}