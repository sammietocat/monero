/**
 * @file    rctplusOps.cpp
 * @brief   set of functions to test RCT related functions in monero
 * */
#include "rctplusOps.hpp"
#include "fmt.hpp"

namespace sammy {
    namespace rctplus {
        using namespace std;

        namespace {
            // max denomination of a coin
            const rct::xmr_amount SRC_DENOMINATION_MAX = 8192;

            const bool INKEY_LOG = false;
            const bool AMOUNT_LOG = false;
            const bool MIXRING_LOG = false;
            const bool RCT_SIG = false;
        }

        /**
         * @brief   check if a `sk` can decrypt the message encrypted by `pk`
         * @details     Check against `sk*G=pk`
         * @param sk    a secret key over ed25519
         * @param pk    a public key over ed25519
         * @return  `true` if `pk=sk*G`, where `G` is the base point of the ed25519
         * */
        bool isPaired(const rct::key &sk, const rct::key &pk) {
            return rct::scalarmultBase(sk) == pk;
        }

        /**
         * @brief   check if two "keys" for RCT is paired
         * @param ctsk      secret key part, in form of `(x,a)`, where `x` is a secret key, and `a` is a mask value in the commitment
         * @param ctpk      public key part, in form of `(P,C)`, where `P=x*G` is the public key, and `C=a*G+amt*H` is the commitment
         * @param amt       amount committed by `(a,C)`
         * @return `true` if the given parameters satisfy `P=x*G` and `C=a*G+amt*H`
         * */
        bool isPaired(const rct::ctkey &ctsk, const rct::ctkey &ctpk, const rct::xmr_amount &amt) {
            return isPaired(ctsk.dest, ctpk.dest) && (rct::commit(amt, ctsk.mask) == ctpk.mask);
        }

        /**
         * @brief  generate amount commitments for an amount set
         * @details     For each amount `amtVec[i]` to commit, generate
         *      * an address key pair `(x,P)` where `P=x*G`, and `P` serves as the address to receive the amount
         *      * a commitment "key pair" `(a,C)`, where `C=a*G+amtVec[i]*H`
         * @param amtVec    a vector containing amounts to commit
         * @return a secret key vector and public key vector bounded with commitments for the amount vector
         * */
        tuple<rct::ctkeyV, rct::ctkeyV> ctskpkVecGen(const vector<rct::xmr_amount> &amtVec) {
            rct::ctkeyV inSk, inPk;
            if (0 != amtVec.size()) {
                inSk.resize(amtVec.size());
                inPk.resize(amtVec.size());

                for (int i = 0; i < amtVec.size(); ++i) {
                    tie(inSk[i], inPk[i]) = rct::ctskpkGen(amtVec[i]);
                }
            }

            return std::make_tuple(inSk, inPk);
        }

        /**
         * @brief   generate a public key vector randomly
         * @param nPk   size of the public key vector to generate
         * @return a random public key vector
         * */
        rct::keyV pkVecGen(const int nPk) {
            rct::keyV pkVec;

            if (0 != nPk) {
                pkVec.reserve(nPk);
                for (int i = 0; i < nPk; ++i) {
                    pkVec.push_back(rct::pkGen());
                }
            }

            return pkVec;
        }

        /**
         * @brief   generate an amount vector randomly, whose accumulated sum is given
		 * @details	The vector is generated in such a way
		 *  * return an empty vector if `n<=0`
		 *  * make a vector `amtVec` of length `n`
		 *  * if `amtSum=0`, generate each amount randomly within range [0,SRC_DENOMINATION_MAX)
		 *  * else
		 *	  * initialize `amtLeft=amtSum`
		 *	  * polish each element of `amtVec_i` from the end to beginning except the 1st one by zero-based index
		 *	    * update `amtVec_i` a random amount within range [0,amtLeft/(i+1))
		 *	    * subtract the amount assigned `amtLeft=amtLeft-amtVec_i`
		 *	  * simply use the rest as the `amtVec_0`
         * @param n     size of the amount vector
         * @param amtSum    sum of the amount in the amount vector
         * @return an amount vector, each element of which is an XMR amount
         * */
        vector<rct::xmr_amount> randomXmrAmountVector(const int n, const rct::xmr_amount amtSum) {
            vector<rct::xmr_amount> amtVec;
            // check size
            if (n <= 0) {
                return amtVec;
            }

            amtVec.resize(n);
            if (0 == amtSum) {
                for (auto &amt:amtVec) {
                    amt = rct::randXmrAmount(SRC_DENOMINATION_MAX);
                }
            } else {
                rct::xmr_amount amtLeft = amtSum;
                for (int j = n - 1; j > 0; --j) {
                    amtVec[j] = rct::randXmrAmount(amtLeft / (j + 1));
                    amtLeft -= amtVec[j];
                }
                amtVec[0] = amtLeft;
            }

            return amtVec;
        }

        /**
         * @brief fake a coins for a wallet
		 * @details This serves to add the extra information to the fake coin include its address and amount
         *  commitment, which are faked as followed
		 *  * derive its address as `(x,P)=(H_s(a*R)+b,x*G)`, where
		 *     * `a` is the secret view key of the owner
		 *     * `R` is the tx's public key
		 *     * `b` is the secret spend key of the owner
		 *  * fake an amount commitment as `(mask,mask*G+amt*H)`, where
		 *    * `mask` is a secret key generated randomly
		 *    * `amt` is the denomination assigned to this coin
         * @param accountKeys   account keys stored in the wallet, containing a public view key and public spend key
         * @param txPk          public key for the transaction containing the coin to fake
         * @param realOutputIdx index of the coin in its containing tx
         * @param amt           amount of the coin to fake
         * @param[out] inSk     secret RCT key for the coin, contains a secret key paired with the address and an amount key for the coin
         * @param[out] inPk     public RCT key for the coin, contains a public key as address and an amount commitment for the coin
         * @return `true` if no error, and `false` otherwise
         * */
        bool fakeWalletSource(const cryptonote::account_keys accountKeys, const crypto::public_key txPk,
                              const size_t realOutputIdx, const rct::xmr_amount amt, rct::ctkey &inSk,
                              rct::ctkey &inPk) {

            cryptonote::keypair inEphemeral;
            crypto::key_image keyImage;
            if (!(cryptonote::generate_key_image_helper(accountKeys, txPk, realOutputIdx, inEphemeral, keyImage))) {
                cerr << " fakeWalletSource: generate_key_image_helper failed" << endl;
                return false;
            }

            inSk.dest = rct::sk2rct(inEphemeral.sec);
            inPk.dest = rct::pk2rct(inEphemeral.pub);

            if (isPaired(inSk.dest, inPk.dest)) {
                LOG_IF(INKEY_LOG, INFO) << "inSk-inPk ok";
            } else {
                LOG_IF(INKEY_LOG, ERROR) << "inSk-inPk bad";
            }

            inSk.mask = rct::skGen();
            inPk.mask = rct::commit(amt, inSk.mask);

            return true;
        }

        /**
         * @brief   generate a set destinations randomly
		 * @details	Make a set of destinations to receive payment as follows
		 *   * ensure `nDsts>0` and `amtSum>0`
		 *   * make a destination vector `dsts` of length `nDsts`
		 *   * initialize amount available as `amtLeft=amtSum`
		 *   * update entry `dsts_i` of `dsts` from the end to beginning except the 1st one
		 *     * give `dsts_i` a random amount within range `(0,amtLeft/(i+1))`
		 *     * randomly generate a public view key and public spend key for it
		 *     * subtract the amount assigned to `dsts_i` from `amtLeft`
		 *  * populate the amount of `dsts_0` as the amount left `amtLeft`, and randomly generate a
         *      public view key and public spend key for it
         * @param nDsts number of destination entries to generate
         * @param amtSum sum of amount sent to these destinations
         * @return the generated set of destination entries ready for tx
         * @note    mainly refers to {@link simplewallet::transfer_main()}
         * */
        vector<cryptonote::tx_destination_entry> randomDestinations(const int nDsts, rct::xmr_amount amtSum) {
            vector<cryptonote::tx_destination_entry> dsts;
            if ((nDsts <= 0) || (0 == amtSum)) {
                return dsts;
            }

            dsts.resize(nDsts);

            rct::xmr_amount amtLeft = amtSum;
            for (int i = nDsts - 1; i > 0; --i) {
                // fill in amount
                do {
                    dsts[i].amount = rct::randXmrAmount(amtLeft / (i + 1));
                } while (0 == dsts[i].amount);
                // populate the public view key
                dsts[i].addr.m_view_public_key = rct::rct2pk(rct::pkGen());
                // populate the public spend key
                dsts[i].addr.m_spend_public_key = rct::rct2pk(rct::pkGen());

                // update the amount available for other destinations
                amtLeft -= dsts[i].amount;

                //cout << "i = " << i << endl;
            }

            // fill in amount
            dsts[0].amount = amtLeft;
            // populate the public view key
            dsts[0].addr.m_view_public_key = rct::rct2pk(rct::pkGen());
            // populate the public spend key
            dsts[0].addr.m_spend_public_key = rct::rct2pk(rct::pkGen());

            return dsts;
        }

        /**
         * @brief   fake a set of coins as inputs for simple RCT
         * @details The source coins minting procedure goes as follows
         *      * ensure
         *          * `nSrc>0`: at least one 1 coin to make
         *          * `amtSum>=nSrc`: each coin should be assigned 1 XMR amount at least
         *          * `mixinCount>0`: at least 1 mix-in for each coin
         *      * delegate to {@link randomXmrAmountVector()} to generate a random vector `amtVec` of amount
         *      * for each coin `src` to generate
         *          * update its amount as that specified by an element at the same index of `amtVec`
         *          * enforce the coin to be used in RCT
         *          * generate randomly a public key as the tx key
		 *          * assign a random index within range [0,63] to the index of coin in its container tx
		 *          * delegate to {@link fakeWalletSource()} to fake the public address for this coin
		 *          * populate the mix-ins vector of length `(mixinCount+1)` by invoking
         *              {@link populateFromBlockchainSimple()} for this coin, each input of which will be
         *              paired with a random global output index, and update the index of real input of `src`
         *              as the feedback by {@link populateFromBlockchainSimple()}
		 *          * update the amount mask (i.e., `a` in the amount commitment `a*G+bH`) for `src`
         *
         * @param accountKeys   public keys allowable to spend coins
         * @param nSrc          number of coins to fake
         * @param amtSum        total amount of all coins
         * @param mixinCount    number of mix-ins for each coin
         * @param[out]  inSkVec secret key vector to spend to coin
         * @return the faked set of coins
         * @note the index of real input for each coin can be different in this scheme. In contrast, faking for full
         *  RCT is implemented by {@link randomTxSourcesFull}
         * */
        vector<cryptonote::tx_source_entry>
        randomTxSourcesSimple(const cryptonote::account_keys &accountKeys, const int nSrc, const rct::xmr_amount amtSum,
                              const int mixinCount, rct::ctkeyV &inSkVec) {
            vector<cryptonote::tx_source_entry> txSources;
            if ((nSrc <= 0) || (amtSum < nSrc) || (0 >= mixinCount)) {
                return txSources;
            }

            // generate a random amount vector
            vector<rct::xmr_amount> amtVec = randomXmrAmountVector(nSrc, amtSum);
            // make room for fake secret keys
            inSkVec.resize(nSrc);
            // make room for tx source entries
            txSources.resize(nSrc);

            // log amounts
            for (int i = 0; i < amtVec.size(); ++i) {
                LOG_IF(AMOUNT_LOG, INFO) << "**amtVec[" << i << "] = " << amtVec[i];
            }

            {
                rct::ctkeyV mixinVec;
                mixinVec.resize(mixinCount + 1);
                uint64_t globalOutputIdx = rct::randXmrAmount(4096);
                rct::ctkey inPkTmp;

                for (int i = 0; i < txSources.size(); ++i) {
                    // amount
                    txSources[i].amount = amtVec[i];
                    // rct switch on
                    txSources[i].rct = true;
                    // tx key
                    txSources[i].real_out_tx_key = rct::rct2pk(rct::pkGen());
                    // output internal index in tx
                    txSources[i].real_output_in_tx_index = rct::randXmrAmount(64);

                    /* fake inputs */
                    fakeWalletSource(accountKeys, txSources[i].real_out_tx_key, txSources[i].real_output_in_tx_index,
                                     amtVec[i], inSkVec[i], inPkTmp);
                    //LOG_IF(!isPaired(inSkVec[i].dest, inPkTmp.dest), ERROR) << "sk-pk mismatched in the fake coin";
                    LOG_IF(!isPaired(inSkVec[i], inPkTmp, amtVec[i]), ERROR)
                        << "ctsk-ctpk is mismatched for the fake coin";
                    /* END fake inputs */

                    // grab mix-ins: actually generate randomly
                    txSources[i].real_output = rct::populateFromBlockchainSimple(mixinVec, inPkTmp, mixinCount);
                    // paste in mix-ins
                    txSources[i].outputs.reserve(mixinVec.size());
                    for (const auto &mixin:mixinVec) {
                        txSources[i].outputs.push_back(std::make_pair(++globalOutputIdx, mixin));
                    }

                    // input amount mask
                    txSources[i].mask = inSkVec[i].mask;
                }
            }

            return txSources;
        }

        /**
         * @brief   fake a set of coins as inputs for full RCT (of one ring only)
         * @details The source coins minting procedure goes as follows
         *      * ensure
         *          * `nSrc>0`: at least one 1 coin to make
         *          * `amtSum>=nSrc`: each coin should be assigned 1 XMR amount at least
         *          * `mixinCount>0`: at least 1 mix-in for each coin
         *      * delegate to {@link randomXmrAmountVector()} to generate a random vector `amtVec` of amount
         *      * for each coin `src` to generate
         *          * update its amount as that specified by an element at the same index of `amtVec`
         *          * enforce the coin to be used in RCT
         *          * generate randomly a public key as the tx key
		 *          * assign a random index within range [0,63] to the index of coin in its container tx
		 *          * delegate to {@link fakeWalletSource()} to fake the address and commitment for this coin
		 *          * update the amount mask (i.e., `a` in the amount commitment `a*G+bH`) for `src`
		 *		* collecting all those public keys in addresses and commitments, mix them with other mix-ins
         *		    generated randomly by {@link rct::populateFromBlockchain()}, and update the index of
         *		    the real input accordingly
         *
         * @param accountKeys   public keys allowable to spend coins
         * @param nSrc          number of coins to fake
         * @param amtSum        total amount of all coins
         * @param mixinCount    number of mix-ins for each coin
         * @param[out]  inSkVec secret key vector to spend to coin
         * @return the faked set of coins
         * @note the index of real input for each coin can be different in this scheme. In contrast, faking for full
         *  RCT is implemented by {@link randomTxSourcesFull}
         * */
        vector<cryptonote::tx_source_entry>
        randomTxSourcesFull(const cryptonote::account_keys &accountKeys, const int nSrc, const rct::xmr_amount amtSum,
                            const int mixinCount, rct::ctkeyV &inSkVec) {
            vector<cryptonote::tx_source_entry> txSources;
            if ((nSrc <= 0) || (amtSum < nSrc) || (0 >= mixinCount)) {
                return txSources;
            }

            // generate a random amount vector
            vector<rct::xmr_amount> amtVec = randomXmrAmountVector(nSrc, amtSum);
            // make room for fake secret keys
            inSkVec.resize(nSrc);
            // make room for tx source entries
            txSources.resize(nSrc);

            // log amounts
            for (int i = 0; i < amtVec.size(); ++i) {
                LOG_IF(AMOUNT_LOG, INFO) << "**amtVec[" << i << "] = " << amtVec[i];
            }

            {
                rct::ctkeyV inPkVec(txSources.size());

                // leave the mix-ins ring first
                for (int i = 0; i < txSources.size(); ++i) {
                    // amount
                    txSources[i].amount = amtVec[i];
                    // rct switch on
                    txSources[i].rct = true;
                    // tx key
                    txSources[i].real_out_tx_key = rct::rct2pk(rct::pkGen());
                    // output internal index in tx
                    txSources[i].real_output_in_tx_index = rct::randXmrAmount(64);

                    /* fake inputs */
                    fakeWalletSource(accountKeys, txSources[i].real_out_tx_key, txSources[i].real_output_in_tx_index,
                                     amtVec[i], inSkVec[i], inPkVec[i]);
                    LOG_IF(!isPaired(inSkVec[i], inPkVec[i], amtVec[i]), ERROR)
                        << "ctsk-ctpk is mismatched for the fake coin";
                    /* END fake inputs */

                    //txSources[i].real_output = rct::populateFromBlockchainSimple(mixinVec, inPkTmp, mixinCount);
                    // paste in mix-ins
                    /*
                    txSources[i].outputs.reserve(mixinVec.size());
                    for (const auto &mixin:mixinVec) {
                        txSources[i].outputs.push_back(std::make_pair(++globalOutputIdx, mixin));
                    }
                     */

                    // input amount mask
                    txSources[i].mask = inSkVec[i].mask;
                }

                // grab mix-ins: actually generate randomly
                uint64_t globalOutputIdx = rct::randXmrAmount(4096);
                rct::ctkeyM mixRing;
                uint64_t index; // index of real inputs
                tie(mixRing, index) = rct::populateFromBlockchain(inPkVec, mixinCount);
                //paste in mix-ins: note transpose operation
                for (int i = 0; i < txSources.size(); ++i) {
                    txSources[i].outputs.reserve(mixRing.size());
                    for (int j = 0; j < mixRing.size(); ++j) {
                        txSources[i].outputs.push_back({++globalOutputIdx, mixRing[j][i]});
                    }
                }
            }

            return txSources;
        }

        /**
         * @brief run a RCT building instance
		 * @details		The instance is built as
		 *		* ensure validity of the input
		 *			* `nSrc`: at least one input coin
		 *			* `amtSum>=nSrc`: at least 1 XMR for each input coin
		 *			* `mixinCount>0`: at least 1 mix-in for each input
		 *			* `nDsts>0`: at least 1 destination to send coin to
		 *		* generate a dummy account
		 *		* fake `nSrc` input coins by invoking {@link randomTxSourcesSimple()}
		 *		* fake `nDsts` destinations
		 *		* make an empty `extra` field and 0 `unlock_time`
		 *		* hand over all the parameters (such as, account keys, input coins, desinations, etc) to
         *		    {@link cryptonote::construct_tx_and_get_tx_key()} to construct a simple RCT `tx`
		 *
         * @param nSrc  number of real input coins
         * @param amtSum    sum of amount of input coins
         * @param mixinCount    number of mix-ins to employ
         * @param nDsts     number of destinations to send coins
         * @param[out] sender    owner/wallet of these coins
         * @param[out] tx       the constructed transaction
         * @param[out] txSecKey secret key of the constructed tx
         * @param[out] fee      fee for the transaction
         * @return  `true` if no error occurs, and `false` otherwise
         * */
        bool randomRCTSimple(const int nSrc, const rct::xmr_amount amtSum, const int mixinCount, const int nDsts,
                             cryptonote::account_base &sender, cryptonote::transaction &tx,
                             crypto::secret_key &txSecKey, const rct::xmr_amount fee) {
            if ((nSrc <= 0) || (amtSum < nSrc) || (mixinCount <= 0) || (nDsts <= 0)) {
                cerr << "invalid configuration: " << endl;
                cerr << "(nSrc=" << nSrc << ", amtSum = " << amtSum << ", mixin = " << mixinCount << ", nDsts = "
                     << nDsts << ")" << endl;
                return false;
            }

            // make a sender account
            sender.generate();
            // random sources for tx
            rct::ctkeyV inSkVec;
            vector<cryptonote::tx_source_entry> txSources = randomTxSourcesSimple(sender.get_keys(), nSrc, amtSum,
                                                                                  mixinCount, inSkVec);
            // print fake secret key vector
            cout << "input secret key vector: " << endl;
            for (const auto &sk:inSkVec) {
                fmt::println(sk);
            }
            cout << "****" << endl;

            // random destinations for tx
            vector<cryptonote::tx_destination_entry> txDestinations = randomDestinations(nDsts, amtSum - fee);
            // empty extra
            vector<uint8_t> extra;
            // dummy unlock time
            uint64_t unlock_time = 0;

            /*
            // construct the tx
            if (rctplus::construct_tx_and_get_tx_key(sender.get_keys(), txSources, txDestinations, extra, tx,
                                                     unlock_time, txSecKey, true)) {
                // check amount
                if (AMOUNT_LOG) {
                    // input amount
                    LOG(INFO) << "**input amounts";
                    rct::xmr_amount inSum = 0;
                    for (int i = 0; i < txSources.size(); ++i) {
                        inSum += txSources[i].amount;
                        LOG(INFO) << "**\t" << i << ": " << txSources[i].amount;
                    }
                    LOG(INFO) << "**\ttotal: " << inSum;

                    // output amount
                    LOG(INFO) << "**output amounts";
                    rct::xmr_amount outSum = 0;
                    for (int i = 0; i < txDestinations.size(); ++i) {
                        outSum += txDestinations[i].amount;
                        LOG(INFO) << "**\t" << i << ": " << txDestinations[i].amount;
                    }
                    LOG(INFO) << "**\ttotal: " << outSum;

                    LOG_IF(inSum != outSum + fee, ERROR) << "input total!=output total+fee(" << fee << ")";
                }

                // check ring signature
                if (RCT_SIG) {
                    rct::rctSig rv = tx.rct_signatures;
                    // check commitment
                    {
                        rct::key pseudoCin = std::accumulate(rv.pseudoOuts.begin(), rv.pseudoOuts.end(),
                                                             rct::identity(),
                                                             [](const rct::key &x, const rct::key &y) {
                                                                 rct::key z;
                                                                 rct::addKeys(z, x, y);
                                                                 return z;
                                                             });
                        rct::key Cout = std::accumulate(rv.outPk.begin(), rv.outPk.end(), rct::identity(),
                                                        [](const rct::key &x, const rct::ctkey &y) {
                                                            rct::key z;
                                                            rct::addKeys(z, x, y.mask);
                                                            return z;
                                                        });
                        // don't miss the commitment for fee
                        rct::key Cfee = rct::scalarmultH(rct::d2h(rv.txnFee));
                        rct::addKeys(Cout, Cout, Cfee);

                        LOG_IF(!(pseudoCin == Cout), ERROR) << "**input commitment != output commitment" << endl;
                    }

                    // check range proof
                    //LOG_IF(!rct::verRctSimple(tx.rct_signatures, true), ERROR) << "**range proof fails";
                    // check MLSAG
                    //LOG_IF(!rct::verRctSimple(tx.rct_signatures, false), ERROR) << "**MLSAG fails";
                }

                return true;
            } else {
                return false;
            }
             */

            return cryptonote::construct_tx_and_get_tx_key(sender.get_keys(), txSources, txDestinations, extra, tx,
                                                           unlock_time, txSecKey, true);
        }

        /**
         * @brief run a full version RCT building instance
		 * @details		The instance is built as
		 *		* ensure validity of the input
		 *			* `nSrc`: at least one input coin
		 *			* `amtSum>=nSrc`: at least 1 XMR for each input coin
		 *			* `mixinCount>0`: at least 1 mix-in for each input
		 *			* `nDsts>0`: at least 1 destination to send coin to
		 *		* generate a dummy account
		 *		* fake `nSrc` input coins by invoking {@link randomTxSourcesFull()}
		 *		* fake `nDsts` destinations
		 *		* make an empty `extra` field and 0 `unlock_time`
		 *		* hand over all the parameters (such as, account keys, input coins, desinations, etc) to
         *		    {@link constructRCTAndGetTxKey()} to construct a full RCT `tx`
		 *
         * @param nSrc  number of real input coins
         * @param amtSum    sum of amount of input coins
         * @param mixinCount    number of mix-ins to employ
         * @param nDsts     number of destinations to send coins
         * @param[out] sender    owner/wallet of these coins
         * @param[out] tx       the constructed transaction
         * @param[out] txSecKey secret key of the constructed tx
         * @param[out] fee      fee for the transaction
         * @return  `true` if no error occurs, and `false` otherwise
         * */
        bool randomRCTFull(const int nSrc, const rct::xmr_amount amtSum, const int mixinCount, const int nDsts,
                           cryptonote::account_base &sender, cryptonote::transaction &tx,
                           crypto::secret_key &txSecKey, const rct::xmr_amount fee) {
            if ((nSrc <= 0) || (amtSum < nSrc) || (mixinCount <= 0) || (nDsts <= 0)) {
                cerr << "invalid configuration: " << endl;
                cerr << "(nSrc=" << nSrc << ", amtSum = " << amtSum << ", mixin = " << mixinCount << ", nDsts = "
                     << nDsts << ")" << endl;
                return false;
            }

            // make a sender account
            sender.generate();
            // random sources for tx
            rct::ctkeyV inSkVec;
            vector<cryptonote::tx_source_entry> txSources = randomTxSourcesFull(sender.get_keys(), nSrc, amtSum,
                                                                                mixinCount, inSkVec);
            // print fake secret key vector
            cout << "input secret key vector: " << endl;
            for (const auto &sk:inSkVec) {
                fmt::println(sk);
            }
            cout << "****" << endl;

            // random destinations for tx
            vector<cryptonote::tx_destination_entry> txDestinations = randomDestinations(nDsts, amtSum - fee);
            // empty extra
            vector<uint8_t> extra;
            // dummy unlock time
            uint64_t unlock_time = 0;

            // construct the tx
            return rctplus::constructRCTAndGetTxKey(sender.get_keys(), txSources, txDestinations, extra,
                                                    rct::RCTTypeFull, tx, unlock_time, txSecKey);
        }

        /**
         * @brief constrcut a RCT tx
         * @see {@link cryptonote::construct_tx_and_get_tx_key()}
         * @note this functions serves the same function as {@link cryptonote::construct_tx_and_get_tx_key()},
         *  and it's modified slightly for testing the full and simple RCT
         * */
        bool constructRCTAndGetTxKey(const cryptonote::account_keys &sender_account_keys,
                                     const std::vector<cryptonote::tx_source_entry> &sources,
                                     const std::vector<cryptonote::tx_destination_entry> &destinations,
                                     const std::vector<uint8_t> extra, const uint8_t rctType,
                                     cryptonote::transaction &tx, uint64_t unlock_time, crypto::secret_key &tx_key) {
            using namespace cryptonote;

            if ((rctType != rct::RCTTypeFull) && (rctType != rct::RCTTypeSimple)) {
                return false;
            }

            std::vector<rct::key> amount_keys;
            tx.set_null();
            amount_keys.clear();

            // enforce RCT
            tx.version = 2;
            tx.unlock_time = unlock_time;

            tx.extra = extra;
            // generate a (r,R) pair as tx key
            keypair txkey = keypair::generate();
            remove_field_from_tx_extra(tx.extra, typeid(tx_extra_pub_key));
            add_tx_pub_key_to_extra(tx, txkey.pub);
            tx_key = txkey.sec;

            // if we have a stealth payment id, find it and encrypt it with the tx key now
            std::vector<tx_extra_field> tx_extra_fields;
            if (parse_tx_extra(tx.extra, tx_extra_fields)) {
                // try a retrieve the nonce if any
                tx_extra_nonce extra_nonce;
                if (find_tx_extra_field_by_type(tx_extra_fields, extra_nonce)) {
                    // retrieve the encrypted payment ID if any
                    crypto::hash8 payment_id = null_hash8;
                    if (get_encrypted_payment_id_from_tx_extra_nonce(extra_nonce.nonce, payment_id)) {
                        LOG_PRINT_L2("Encrypting payment id " << payment_id);
                        crypto::public_key view_key_pub = get_destination_view_key_pub(destinations,
                                                                                       sender_account_keys);
                        if (view_key_pub == null_pkey) {
                            LOG_ERROR("Destinations have to have exactly one output to support encrypted payment ids");
                            return false;
                        }

                        if (!encrypt_payment_id(payment_id, view_key_pub, txkey.sec)) {
                            LOG_ERROR("Failed to encrypt payment id");
                            return false;
                        }

                        std::string extra_nonce;
                        set_encrypted_payment_id_to_tx_extra_nonce(extra_nonce, payment_id);
                        remove_field_from_tx_extra(tx.extra, typeid(tx_extra_nonce));
                        if (!add_extra_nonce_to_tx_extra(tx.extra, extra_nonce)) {
                            LOG_ERROR("Failed to add encrypted payment id to tx extra");
                            return false;
                        }
                        LOG_PRINT_L1("Encrypted payment ID: " << payment_id);
                    }
                }
            } else {
                LOG_ERROR("Failed to parse tx extra");
                return false;
            }

            // container for an ephemeral key pair storing the shared ECDH keys
            struct input_generation_context_data {
                keypair in_ephemeral;
            };
            std::vector<input_generation_context_data> in_contexts; // a vector of ephemeral key pairs

            uint64_t summary_inputs_money = 0;
            //fill inputs
            int idx = -1;
            for (const tx_source_entry &src_entr:  sources) {
                ++idx;
                // check the validity of the real input coin in the source ring
                if (src_entr.real_output >= src_entr.outputs.size()) {
                    LOG_ERROR("real_output index (" << src_entr.real_output << ")bigger than output_keys.size()="
                                                    << src_entr.outputs.size());
                    return false;
                }
                summary_inputs_money += src_entr.amount;

                //key_derivation recv_derivation;
                in_contexts.push_back(input_generation_context_data());
                keypair &in_ephemeral = in_contexts.back().in_ephemeral;
                crypto::key_image img;
                if (!generate_key_image_helper(sender_account_keys, src_entr.real_out_tx_key,
                                               src_entr.real_output_in_tx_index, in_ephemeral, img))
                    return false;

                //check that derivated key is equal with real output key
                if (!(in_ephemeral.pub == src_entr.outputs[src_entr.real_output].second.dest)) {
                    LOG_ERROR("derived public key mismatch with output public key at index " << idx << ", real out "
                                                                                             << src_entr.real_output
                                                                                             << "! "
                                                                                             << ENDL << "derived_key:"
                                                                                             << epee::string_tools::pod_to_hex(
                                                                                                     in_ephemeral.pub)
                                                                                             << ENDL
                                                                                             << "real output_public_key:"
                                                                                             << epee::string_tools::pod_to_hex(
                                                                                                     src_entr.outputs[src_entr.real_output].second));
                    LOG_ERROR("amount " << src_entr.amount << ", rct " << src_entr.rct);
                    LOG_ERROR("tx pubkey " << src_entr.real_out_tx_key << ", real_output_in_tx_index "
                                           << src_entr.real_output_in_tx_index);
                    return false;
                }

                //put key image into tx input
                txin_to_key input_to_key;
                input_to_key.amount = src_entr.amount;
                input_to_key.k_image = img;

                //fill outputs array and use relative offsets
                // fill in the absolute offsets first
                for (const tx_source_entry::output_entry &out_entry: src_entr.outputs)
                    input_to_key.key_offsets.push_back(out_entry.first);

                // transform absolute offsets into relative ones
                input_to_key.key_offsets = absolute_output_offsets_to_relative(input_to_key.key_offsets);
                /* done fill in relative offsets */

                // add the input to tx
                tx.vin.push_back(input_to_key);
            }

            // "Shuffle" outs (i.e., sort them in ascending order of their output amount)
            std::vector<tx_destination_entry> shuffled_dsts(destinations);
            std::sort(shuffled_dsts.begin(), shuffled_dsts.end(),
                      [](const tx_destination_entry &de1, const tx_destination_entry &de2) {
                          return de1.amount < de2.amount;
                      });

            uint64_t summary_outs_money = 0;
            //fill outputs
            size_t output_index = 0;
            for (const tx_destination_entry &dst_entr:  shuffled_dsts) {
                // if amount>0, it should be a non-RCT (i.e., v1)
                // if amount=0, it should be a RCT (i.e., v2)
                CHECK_AND_ASSERT_MES(dst_entr.amount > 0 || tx.version > 1, false,
                                     "Destination with wrong amount: " << dst_entr.amount);
                /* to derive the one-time key */
                crypto::key_derivation derivation;
                // buffer to hold up the ephemeral key
                crypto::public_key out_eph_public_key;
                // derivation = sec*m_view_public_key
                bool r = crypto::generate_key_derivation(dst_entr.addr.m_view_public_key, txkey.sec, derivation);
                CHECK_AND_ASSERT_MES(r, false, "at creation outs: failed to generate_key_derivation("
                        << dst_entr.addr.m_view_public_key << ", " << txkey.sec << ")");

                if (tx.version > 1) {
                    // compute the amount key (H_s(derivation||output_index)) for the output
                    crypto::secret_key scalar1;
                    crypto::derivation_to_scalar(derivation, output_index, scalar1);
                    amount_keys.push_back(rct::sk2rct(scalar1));
                }
                // out_eph_public_key=H_s(derivation||output_index)*G+B
                r = crypto::derive_public_key(derivation, output_index, dst_entr.addr.m_spend_public_key,
                                              out_eph_public_key);
                CHECK_AND_ASSERT_MES(r, false,
                                     "at creation outs: failed to derive_public_key(" << derivation << ", "
                                                                                      << output_index
                                                                                      << ", "
                                                                                      << dst_entr.addr.m_spend_public_key
                                                                                      << ")");

                tx_out out;
                out.amount = dst_entr.amount;
                txout_to_key tk;
                tk.key = out_eph_public_key;
                out.target = tk;
                tx.vout.push_back(out);
                output_index++;
                summary_outs_money += dst_entr.amount;
            } // end  `dst_entr` loop

            //check money
            if (summary_outs_money > summary_inputs_money) {
                LOG_ERROR("Transaction inputs money (" << summary_inputs_money << ") less than outputs money ("
                                                       << summary_outs_money << ")");
                return false;
            }

            // check for watch only wallet
            bool zero_secret_key = true;
            for (size_t i = 0; i < sizeof(sender_account_keys.m_spend_secret_key); ++i)
                zero_secret_key &= (sender_account_keys.m_spend_secret_key.data[i] == 0);
            if (zero_secret_key) {
                MDEBUG("Null secret key, skipping signatures");
            }

            if (tx.version == 1) {
                //generate ring signatures
                crypto::hash tx_prefix_hash;
                get_transaction_prefix_hash(tx, tx_prefix_hash);

                std::stringstream ss_ring_s;
                size_t i = 0;
                for (const tx_source_entry &src_entr:  sources) {
                    ss_ring_s << "pub_keys:" << ENDL;
                    std::vector<const crypto::public_key *> keys_ptrs;
                    std::vector<crypto::public_key> keys(src_entr.outputs.size());
                    size_t ii = 0;
                    for (const tx_source_entry::output_entry &o: src_entr.outputs) {
                        keys[ii] = rct2pk(o.second.dest);
                        keys_ptrs.push_back(&keys[ii]);
                        ss_ring_s << o.second.dest << ENDL;
                        ++ii;
                    }

                    tx.signatures.push_back(std::vector<crypto::signature>());
                    std::vector<crypto::signature> &sigs = tx.signatures.back();
                    sigs.resize(src_entr.outputs.size());
                    if (!zero_secret_key)
                        crypto::generate_ring_signature(tx_prefix_hash, boost::get<txin_to_key>(tx.vin[i]).k_image,
                                                        keys_ptrs, in_contexts[i].in_ephemeral.sec,
                                                        src_entr.real_output,
                                                        sigs.data());
                    ss_ring_s << "signatures:" << ENDL;
                    std::for_each(sigs.begin(), sigs.end(),
                                  [&](const crypto::signature &s) { ss_ring_s << s << ENDL; });
                    ss_ring_s << "prefix_hash:" << tx_prefix_hash << ENDL << "in_ephemeral_key: "
                              << in_contexts[i].in_ephemeral.sec << ENDL << "real_output: " << src_entr.real_output
                              << ENDL;
                    i++;
                }

                MCINFO("construct_tx",
                       "transaction_created: " << get_transaction_hash(tx) << ENDL << obj_to_json_str(tx) << ENDL
                                               << ss_ring_s.str());
            } else {
                size_t n_total_outs = sources[0].outputs.size(); // only for non-simple rct

                // the non-simple version is slightly smaller, but assumes all real inputs
                // are on the same index, so can only be used if there just one ring.
                // comment out by sammietocat at 2017-08-10
                //bool use_simple_rct = sources.size() > 1;
                bool use_simple_rct = (rct::RCTTypeSimple == rctType);

                if (!use_simple_rct) {
                    // size of source isn't 1 here? why still check
                    // non simple ringct requires all real inputs to be at the same index for all inputs
                    for (const tx_source_entry &src_entr:  sources) {
                        if (src_entr.real_output != sources.begin()->real_output) {
                            LOG_ERROR("All inputs must have the same index for non-simple ringct");
                            return false;
                        }
                    }

                    // enforce same mixin for all outputs
                    for (size_t i = 1; i < sources.size(); ++i) {
                        if (n_total_outs != sources[i].outputs.size()) {
                            LOG_ERROR("Non-simple ringct transaction has varying mixin");
                            return false;
                        }
                    }
                }

                uint64_t amount_in = 0, amount_out = 0;
                rct::ctkeyV inSk;
                // mixRing indexing is done the other way round for simple
                rct::ctkeyM mixRing(use_simple_rct ? sources.size() : n_total_outs);
                rct::keyV destinations;
                std::vector<uint64_t> inamounts, outamounts;
                std::vector<unsigned int> index;
                /* build the real input secret key vector */
                for (size_t i = 0; i < sources.size(); ++i) {
                    rct::ctkey ctkey;
                    amount_in += sources[i].amount;
                    inamounts.push_back(sources[i].amount);
                    index.push_back(sources[i].real_output);
                    // inSk: (secret key, mask)
                    ctkey.dest = rct::sk2rct(in_contexts[i].in_ephemeral.sec);
                    ctkey.mask = sources[i].mask;
                    inSk.push_back(ctkey);
                    // inPk: (public key, commitment)
                    // will be done when filling in mixRing
                }
                /* end build the real input secret key vector */
                /* build the output public key vector, i.e., addresses to send coins to */
                for (size_t i = 0; i < tx.vout.size(); ++i) {
                    destinations.push_back(rct::pk2rct(boost::get<txout_to_key>(tx.vout[i].target).key));
                    outamounts.push_back(tx.vout[i].amount);
                    amount_out += tx.vout[i].amount;
                }
                /* end build the output public key vector, i.e., addresses to send coins to */

                if (use_simple_rct) {
                    // mixRing indexing is done the other way round for simple
                    for (size_t i = 0; i < sources.size(); ++i) {
                        mixRing[i].resize(sources[i].outputs.size());
                        for (size_t n = 0; n < sources[i].outputs.size(); ++n) {
                            mixRing[i][n] = sources[i].outputs[n].second;
                        }
                    }
                } else {
                    for (size_t i = 0; i < n_total_outs; ++i) // same index assumption
                    {
                        mixRing[i].resize(sources.size());
                        for (size_t n = 0; n < sources.size(); ++n) {
                            mixRing[i][n] = sources[n].outputs[i].second;
                        }
                    }
                }

                // fee
                if (!use_simple_rct && amount_in > amount_out)
                    outamounts.push_back(amount_in - amount_out);

                // zero out all amounts to mask rct outputs, real amounts are now encrypted
                for (size_t i = 0; i < tx.vin.size(); ++i) {
                    if (sources[i].rct)
                        boost::get<txin_to_key>(tx.vin[i]).amount = 0;
                }
                for (size_t i = 0; i < tx.vout.size(); ++i)
                    tx.vout[i].amount = 0;

                crypto::hash tx_prefix_hash;
                get_transaction_prefix_hash(tx, tx_prefix_hash);
                rct::ctkeyV outSk;
                if (use_simple_rct)
                    tx.rct_signatures = rct::genRctSimple(rct::hash2rct(tx_prefix_hash), inSk, destinations, inamounts,
                                                          outamounts, amount_in - amount_out, mixRing, amount_keys,
                                                          index,
                                                          outSk);
                else
                    tx.rct_signatures = rct::genRct(rct::hash2rct(tx_prefix_hash), inSk, destinations, outamounts,
                                                    mixRing,
                                                    amount_keys, sources[0].real_output,
                                                    outSk); // same index assumption

                CHECK_AND_ASSERT_MES(tx.vout.size() == outSk.size(), false, "outSk size does not match vout");

                MCINFO("construct_tx",
                       "transaction_created: " << get_transaction_hash(tx) << ENDL << obj_to_json_str(tx) << ENDL);
            }

            tx.invalidate_hashes();

            return true;
        }
    }
};