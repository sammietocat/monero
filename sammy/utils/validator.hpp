#pragma once
#ifndef _VALIDATOR_H_
#define _VALIDATOR_H_
#include "ringct/rctOps.h"

namespace sammy {
    namespace validator {
        bool isPaired(const rct::key &sk, const rct::key &pk) {
            return rct::scalarmultBase(sk) == pk;
        }
    };
};

#endif
