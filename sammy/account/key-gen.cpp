#include <iostream>

#include "hex.h"
#include "ringct/rctOps.h"

using namespace std;

int main(int argc, char *argv[]) {
    cout << "Hello World"<<endl;

    rct::key k = rct::skGen();
    epee::to_hex::buffer(std::cout, k.bytes);
    cout<<endl;

    return 0;
}