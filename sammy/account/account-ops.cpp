#include <iostream>

#include "hex.h"    // from epee
#include "cryptonote_basic/account.h"   // from cryptonote
#include "ringct/rctOps.h"      // from ringct
#include "ringct/rctTypes.h"    // from ringct

using namespace std;

namespace cn = cryptonote;

int main(int argc, char *argv[]) {
    cn::account_base payer;
    crypto::secret_key rk = payer.generate();

    // print account info of the payer
    cout << "key for recovery is "<<endl;
    cout <<"\t";
    epee::to_hex::buffer(std::cout, rct::sk2rct(rk).bytes);
    cout <<endl;

    cout << "creation timestamp is "<<endl;
    cout << "\t"<<payer.get_createtime()<<endl;

    const cn::account_keys ack = payer.get_keys();
    /** address **/
    // view key
    cout << "view keys: "<<endl;
    cout <<"\t secret part: ";
    epee::to_hex::buffer(std::cout, rct::sk2rct(ack.m_view_secret_key).bytes);
    cout <<endl;
    cout <<"\t public part: ";
    epee::to_hex::buffer(std::cout, rct::pk2rct(ack.m_account_address.m_view_public_key).bytes);
    cout <<endl;

    // spend key
    cout << "spend keys: "<<endl;
    cout <<"\t secret part: ";
    epee::to_hex::buffer(std::cout, rct::sk2rct(ack.m_spend_secret_key).bytes);
    cout <<endl;
    cout <<"\t public part: ";
    epee::to_hex::buffer(std::cout, rct::pk2rct(ack.m_account_address.m_spend_public_key).bytes);
    cout <<endl;

    cout << "expected public view key is "<<endl;
    cout << "\t";
    epee::to_hex::buffer(std::cout, rct::scalarmultBase(rct::sk2rct(ack.m_view_secret_key)).bytes);
    cout<<endl;
    cout << "expected public spend key is "<<endl;
    cout << "\t";
    epee::to_hex::buffer(std::cout, rct::scalarmultBase(rct::sk2rct(ack.m_spend_secret_key)).bytes);
    cout<<endl;
    /** address **/

    return 0;
}