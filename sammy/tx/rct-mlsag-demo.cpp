/**
 * @file    rct-mg-demo.cpp
 * @brief   demo of simple MLSAG signing
 * @date    2017-08-08
 * */
#include <iostream>

#include "cryptonote_basic/cryptonote_format_utils.h" //from cryptonote_basic
#include "ringct/rctSigs.h" // from ringct

using namespace std;

int main(int argc, char *argv[]) {
    const rct::key MESSAGE = rct::skGen();
    const rct::xmr_amount AMOUNT = 4096 + rct::randXmrAmount(8192);
    const int MIX_IN = 4;

    // input keys
    rct::ctkey inSk, inPk;
    // output mask and commitment
    rct::key a, Cout;

    tie(inSk, inPk) = rct::ctskpkGen(AMOUNT);

    // fake a output
    a = rct::skGen();
    Cout = rct::commit(AMOUNT, a);

    rct::ctkeyV mixinVec;
    mixinVec.resize(MIX_IN + 1);

    uint64_t realInIdx = rct::populateFromBlockchainSimple(mixinVec, inPk, MIX_IN);
    rct::mgSig sig = rct::proveRctMGSimple(MESSAGE, mixinVec, inSk, a, Cout, realInIdx);
    cout << "The generated simple MLSAG is " << endl;
    cout << cryptonote::obj_to_json_str(sig) << endl;

    // verify the signature
    if (rct::verRctMGSimple(MESSAGE, sig, mixinVec, Cout)) {
        cout << "rct::verRctMGSimple: the signature is ok" << endl;
    } else {
        cerr << "rct::verRctMGSimple: the signature is not ok" << endl;
    }

    return 0;
}