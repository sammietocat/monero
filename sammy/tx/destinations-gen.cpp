//
// Created by loccs on 8/7/17.
//

#include <iostream>

//#include
#include "rctplusOps.hpp"
#include "fmt.hpp"

using namespace std;
using namespace sammy;

int main(int argc, char *argv[]) {
    typedef cryptonote::tx_destination_entry dstType;

    rct::xmr_amount totalAmount = 4096 + rct::randXmrAmount(4096);

    const int DEST_SIZE = 8;

    vector <dstType> dsts = rctplus::randomDestinations(DEST_SIZE, totalAmount);

    rct::xmr_amount dstsSum = 0;
    for (const auto &dst:dsts) {
        fmt::println(dst);
        dstsSum += dst.amount;
    }

    cout << totalAmount << "==" << dstsSum << "? " << (totalAmount == dstsSum) << endl;

    return 0;
}