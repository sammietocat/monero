#include <iostream>

#include "cryptonote_core/cryptonote_tx_utils.h" // cryptonote_core
#include "ringct/rctSigs.h" // ringct

#include "fmt.hpp"
#include "rctplusOps.hpp"

using namespace sammy;
using namespace std;

int main(int argc, char *argv[]) {
    /** test commitment on an amount**/
    const rct::xmr_amount b = rct::randXmrAmount(1024);

    rct::ctkey x, P;
    std::tie(x, P) = rct::ctskpkGen(b);

    cout << "secret key and public key for address are matched? " << (rctplus::isPaired(x.dest, P.dest) ? "yes" : "no")
         << endl;

    rct::key P2 = rct::commit(b, x.mask);
    cout << "secret key and public key for mask are matched? " << (P2 == P.mask ? "yes" : "no")
         << endl;

    cout << "**********check zero commitment" << endl;
    rct::key C0 = rct::zeroCommit(b);
    rct::key bH = rct::scalarmultH(rct::d2h(b));
    rct::key IGbH, IH;

    rct::addKeys1(IGbH, rct::I, bH);
    IH = rct::scalarmultH(rct::I);

    cout << "C0==b*H ? " << (C0 == bH) << endl;
    cout << "C0==I*G+b*H ? " << (C0 == IGbH) << endl;
    cout << "H==I*H ? " << (rct::H == IH) << endl;
    /** END test commitment on an amount**/

    return 0;
}
