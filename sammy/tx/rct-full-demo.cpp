//
// Created by loccs on 8/8/17.
//
#include <iostream>

#include "rctplusOps.hpp"   // from utils

using namespace sammy;
using namespace std;

int main(int argc, char *argv[]) {
    const int SOURCE_SIZE = 4;
    const rct::xmr_amount AMOUNT = rct::randXmrAmount(8192);
    const int DESTINATION_SIZE = 5;
    const unsigned int MIX_IN = 6;

    // fake the message to sign
    rct::key msg = rct::skGen();

    // fake some random amount
    vector <rct::xmr_amount> inAmtVec = rctplus::randomXmrAmountVector(SOURCE_SIZE, AMOUNT);
    LOG_IF(AMOUNT != std::accumulate(inAmtVec.begin(), inAmtVec.end(), 0), ERROR) << "***invalid input amount vectors";

    // fake the corresponding secret keys and commitment, etc
    rct::ctkeyV inSk, inPk;
    tie(inSk, inPk) = rctplus::ctskpkVecGen(inAmtVec);

    // fake some destinations
    rct::keyV destinations = rctplus::pkVecGen(DESTINATION_SIZE);

    // tx fee
    const rct::xmr_amount TX_FEE = rct::randXmrAmount(64);

    // fake amounts for destinations
    vector <rct::xmr_amount> outAmtVec = rctplus::randomXmrAmountVector(destinations.size(), AMOUNT - TX_FEE);
    LOG_IF(AMOUNT - TX_FEE != std::accumulate(outAmtVec.begin(), outAmtVec.end(), 0), ERROR)
            << "***invalid output amount vectors";

    // append tx fee to output amount vector to adapt for the {@link rct::genRct()}
    outAmtVec.push_back(TX_FEE);

    // fake shared secret keys for encrypting amounts sent to destinations
    rct::keyV sharedSecVec = rct::skvGen(destinations.size());

    uint64_t index;  // index for real input
    rct::ctkeyM mixRing;
    rct::ctkeyV outSk;

    // "fetch" mix-ins from blockchain
    tie(mixRing, index) = rct::populateFromBlockchain(inPk, MIX_IN);
    // check real pubkeys in the mix-ins matrix
    LOG_IF(inPk.size() != mixRing[index].size(), ERROR) << "invalid mixRing";
    for (int i = 0; i < inPk.size(); ++i) {
        LOG_IF(!(inPk[i].dest == mixRing[index][i].dest) || !(inPk[i].mask == mixRing[index][i].mask), ERROR) << "inPk["
                                                                                                              << i
                                                                                                              << "] not matched with mixRing["
                                                                                                              << index
                                                                                                              << "]["
                                                                                                              << i
                                                                                                              << "]";
    }

    rct::rctSig rv = rct::genRct(msg, inSk, destinations, outAmtVec, mixRing, sharedSecVec, index, outSk);

    // check tx fee
    LOG_IF(TX_FEE != rv.txnFee, ERROR) << "TX_FEE(" << TX_FEE << ")!=" << "rv.txnFee(" << rv.txnFee << ")";

    //LOG_IF(!rct::verRctSimple(rv, true), ERROR) << "rct::verRctSimple(rv,true) to verify range proof is not ok";
    //LOG_IF(!rct::verRctSimple(rv, false), ERROR) << "rct::verRctSimple(rv,true) to verify MLSAGs is not ok";
    LOG_IF(!rct::verRct(rv, true), ERROR) << "rct::verRct(rv,true) to verify range proof is not ok";
    LOG_IF(!rct::verRct(rv, false), ERROR) << "rct::verRct(rv,true) to verify MLSAGs is not ok";

    // no pseudo input commitments any more
    // real input commitments
    rct::key Cin = rct::identity();
    for (const auto &pk:rv.mixRing[index]) {
        rct::addKeys(Cin, Cin, pk.mask);
    }
    // z
    rct::key z;
    sc_0(z.bytes);
    // add up input mask values
    for (const auto &sk:inSk) {
        sc_add(z.bytes, z.bytes, sk.mask.bytes);
    }
    // subtract output mask values
    for (const auto &sk:outSk) {
        sc_sub(z.bytes, z.bytes, sk.mask.bytes);
    }

    // sum of output commitments
    rct::key Cout = rct::identity();
    for (const auto &out:rv.outPk) {
        rct::addKeys(Cout, Cout, out.mask);
    }

    // don't miss the fee
    rct::key Cfee = rct::scalarmultH(rct::d2h(rv.txnFee));
    /*
    rct::key Cfee2 = rct::zeroCommit(rv.txnFee);
    rct::subKeys(Cfee2,Cfee2,rct::scalarmultBase(rct::I));
    LOG_IF(!(Cfee == Cfee2), ERROR) << "Cfee != Cfee2";
     */
    // plus the fee
    rct::addKeys(Cout, Cout, Cfee);
    // plus z*G
    rct::addKeys(Cout, Cout, rct::scalarmultBase(z));

    LOG_IF(!(Cin == Cout), ERROR) << "pseudoCin != Cout";

    cout << "so far so good" << endl;

    return 0;
}
