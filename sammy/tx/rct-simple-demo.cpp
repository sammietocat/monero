//
// Created by loccs on 8/8/17.
//
#include <iostream>
#include <numeric>

#include "rctplusOps.hpp"   // from utils

using namespace sammy;
using namespace std;

int main(int argc, char *argv[]) {
    const int SOURCE_SIZE = 4;
    const rct::xmr_amount AMOUNT = rct::randXmrAmount(8192);
    const int DESTINATION_SIZE = 5;
    const unsigned int MIX_IN = 6;

    // fake the message to sign
    rct::key msg = rct::skGen();

    // fake some random amount
    vector<rct::xmr_amount> inAmtVec = rctplus::randomXmrAmountVector(SOURCE_SIZE, AMOUNT);
    LOG_IF(AMOUNT != std::accumulate(inAmtVec.begin(), inAmtVec.end(), 0), ERROR) << "***invalid input amount vectors";

    // fake the corresponding secret keys and commitment, etc
    rct::ctkeyV inSk, inPk;
    tie(inSk, inPk) = rctplus::ctskpkVecGen(inAmtVec);

    // fake some destinations
    rct::keyV destinations = rctplus::pkVecGen(DESTINATION_SIZE);

    // tx fee
    const rct::xmr_amount TX_FEE = rct::randXmrAmount(64);

    // fake amounts for destinations
    vector<rct::xmr_amount> outAmtVec = rctplus::randomXmrAmountVector(destinations.size(), AMOUNT - TX_FEE);
    LOG_IF(AMOUNT - TX_FEE != std::accumulate(outAmtVec.begin(), outAmtVec.end(), 0), ERROR)
        << "***invalid input amount vectors";

    // fake shared secret keys for encrypting amounts sent to destinations
    rct::keyV sharedSecVec = rct::skvGen(destinations.size());

    //rct::rctSig rv = rct::genRctSimple(msg, inSk, inPk, destinations, inAmtVec, outAmtVec, amtKeyVec, TX_FEE, MIX_IN);
    vector<uint32_t> indices;
    rct::ctkeyM mixRing;
    rct::ctkeyV outSk;

    indices.resize(inPk.size());
    mixRing.resize(inPk.size());
    for (size_t i = 0; i < inPk.size(); ++i) {
        mixRing[i].resize(MIX_IN + 1);
        indices[i] = rct::populateFromBlockchainSimple(mixRing[i], inPk[i], MIX_IN);
    }

    rct::rctSig rv = rct::genRctSimple(msg, inSk, destinations, inAmtVec, outAmtVec, TX_FEE, mixRing, sharedSecVec,
                                       indices, outSk);

    LOG_IF(!rct::verRctSimple(rv, true), ERROR) << "rct::verRctSimple(rv,true) to verify range proof is not ok";
    LOG_IF(!rct::verRctSimple(rv, false), ERROR) << "rct::verRctSimple(rv,true) to verify MLSAGs is not ok";

    rct::key pseudoCin = std::accumulate(rv.pseudoOuts.begin(), rv.pseudoOuts.end(), rct::identity(),
                                         [](const rct::key &x, const rct::key &y) {
                                             rct::key z;
                                             rct::addKeys(z, x, y);
                                             return std::move(z);
                                         });

    rct::key Cout = std::accumulate(rv.outPk.begin(), rv.outPk.end(), rct::identity(),
                                    [](const rct::key &x, const rct::ctkey &y) {
                                        rct::key z;
                                        rct::addKeys(z, x, y.mask);
                                        return std::move(z);
                                    });

    // don't miss the fee
    rct::key Cfee = rct::scalarmultH(rct::d2h(rv.txnFee));
    /*
    rct::key Cfee2 = rct::zeroCommit(rv.txnFee);
    rct::subKeys(Cfee2,Cfee2,rct::scalarmultBase(rct::I));
    LOG_IF(!(Cfee == Cfee2), ERROR) << "Cfee != Cfee2";
     */

    rct::addKeys(Cout, Cout, Cfee);

    LOG_IF(!(pseudoCin == Cout), ERROR) << "pseudoCin != Cout";

    cout << "so far so good" << endl;

    return 0;
}
