//
// Created by loccs on 8/7/17.
//

#include <iostream>
#include <cryptonote_basic/account.h>

#include "rctplusOps.hpp"

using namespace sammy;
using namespace std;

int main(int argc, char *argv[]) {
    el::Configurations rctplusConf;
    rctplusConf.setToDefault();
    rctplusConf.set(el::Level::Info, el::ConfigurationType::Format, "%datetime %level %loc - %msg");
    el::Loggers::reconfigureLogger("default", rctplusConf);

    const rct::xmr_amount TX_AMOUNT = 8192;
    const rct::xmr_amount TX_FEE = rct::randXmrAmount(64);
    const int SOURCE_SIZE = 4;
    const int MIX_IN = 3;
    const int DEST_SIZE = 8;

    cryptonote::account_base sender;
    cryptonote::transaction tx;
    crypto::secret_key txSecKey;

    // generate the tx
    LOG_IF(!rctplus::randomRCTFull(SOURCE_SIZE, TX_AMOUNT, MIX_IN, DEST_SIZE, sender, tx, txSecKey, TX_FEE), ERROR)
            << "Full RCT fails";

    // verify the range proof
    LOG_IF(!rct::verRct(tx.rct_signatures, true), ERROR) << "rct::verRctSimple for signature failed";
    // verify the MLSAG part in the ring signature
    LOG_IF(!rct::verRct(tx.rct_signatures, false), ERROR) << "rct::verRctSimple for signature failed";

    LOG(INFO) << "so far so good";

    return 0;
}