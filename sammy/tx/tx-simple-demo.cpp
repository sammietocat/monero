//
// Created by loccs on 8/7/17.
//

#include <iostream>
#include <cryptonote_basic/account.h>

#include "rctplusOps.hpp"

using namespace sammy;
using namespace std;

int main(int argc, char *argv[]) {
    el::Configurations rctplusConf;
    rctplusConf.setToDefault();
    rctplusConf.set(el::Level::Info, el::ConfigurationType::Format, "%datetime %level %loc - %msg");
    el::Loggers::reconfigureLogger("default", rctplusConf);

    const rct::xmr_amount TX_AMOUNT = 8192;
    const rct::xmr_amount TX_FEE = rct::randXmrAmount(64);
    const int SOURCE_SIZE = 4;
    const int MIX_IN = 3;
    const int DEST_SIZE = 8;

    cryptonote::account_base sender;
    cryptonote::transaction tx;
    crypto::secret_key txSecKey;

    // generate the tx
    if (rctplus::randomRCTSimple(SOURCE_SIZE, TX_AMOUNT, MIX_IN, DEST_SIZE, sender, tx, txSecKey, TX_FEE)) {
        cout << "Simple RCT ok" << endl;
    } else {
        cerr << "Simple RCT fails" << endl;
    }

    /*
    // verify the range
    if (rct::verRctSimple(tx.rct_signatures, true)) {
        cout << "rct::verRctSimple for range ok" << endl;
    } else {
        cerr << "rct::verRctSimple for range failed" << endl;
    }
    */

    // check the signed message
    /*
    const rct::rctSig rv = tx.rct_signatures;
    const rct::key message = rctplus::get_pre_mlsag_hash(rv);
    const int MGIdx = 0;
    LOG(INFO) << "**" << epee::to_hex::string(message.bytes);

    LOG(INFO) << "**" << MGIdx << "st mix-ins ring";
    for (const auto &m:rv.mixRing[MGIdx]) {
        LOG(INFO) << "**" << epee::to_hex::string(m.dest.bytes);
    }
    LOG(INFO) << "**END: " << MGIdx << "st mix-ins ring";
     */

    /*
    if (rct::verRctMGSimple(message, rv.p.MGs[MGIdx], rv.mixRing[MGIdx], rv.pseudoOuts[MGIdx])) {
        LOG(INFO) << "**MGs[0] ok" << endl;
    } else {
        LOG(ERROR) << "**MGs[0] not ok" << endl;
    }
     */

    // verify the range proof
    LOG_IF(!rct::verRctSimple(tx.rct_signatures, true), ERROR) << "rct::verRctSimple for signature failed";
    // verify the MLSAG part in the ring signature
    LOG_IF(!rct::verRctSimple(tx.rct_signatures, false), ERROR) << "rct::verRctSimple for signature failed";

    LOG(INFO) << "so far so good";

    return 0;
}