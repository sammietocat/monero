#include <iostream>

#include "fmt.hpp"
#include "rctplusOps.hpp"

using namespace sammy;
using namespace std;

int main(int argc, char *argv[]) {
    const rct::xmr_amount TX_AMOUNT = 8096;
    const int SOURCE_SIZE = 4;
    const int MIX_IN = 5;

    // make a dummy account
    cryptonote::account_base sender;
    sender.generate();
    // container for the secret key vector for the generated source entries
    rct::ctkeyV inSkVec;

    vector <cryptonote::tx_source_entry> sources = rctplus::randomTxSourcesFull(sender.get_keys(), SOURCE_SIZE,
                                                                                TX_AMOUNT, MIX_IN, inSkVec);
    cout << "account info as " << endl;
    const cryptonote::account_keys &ack = sender.get_keys();
    cout << " viewing sk: " << fmt::to_string(rct::sk2rct(ack.m_view_secret_key)) << endl;
    cout << " viewing pk: " << fmt::to_string(rct::pk2rct(ack.m_account_address.m_view_public_key)) << endl;
    cout << "spending sk: " << fmt::to_string(rct::sk2rct(ack.m_spend_secret_key)) << endl;
    cout << "spending pk: " << fmt::to_string(rct::pk2rct(ack.m_account_address.m_spend_public_key)) << endl;

    cout << "\nsource entries as " << endl;
    for (const auto &src:sources) {
        fmt::println(src);
    }

    cout << "\nsecret key vector as " << endl;
    for (const auto &inSk:inSkVec) {
        fmt::println(inSk);
    }

    return 0;
}
