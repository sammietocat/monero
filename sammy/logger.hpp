#pragma once 
#include <iostream>

using namespace std;

namespace sammy {
    const string SAMMY_LOGGER = "sammy> ";
    const string SAMMY_STAR_LINE = "**************************************";

    template<typename M, typename DESC>
    void info(M msg, DESC desc, const char *fd=__FILE__, const int ln=__LINE__) {
        cout << SAMMY_STAR_LINE<<endl;
        cout << SAMMY_LOGGER<<fd<<" ("<<ln<<"): "<<msg<<"--"<<desc<<endl;
        cout << SAMMY_STAR_LINE<<endl;
    }

    #define INFO(x,y) info(x,y,__FILE__, __LINE__);
}